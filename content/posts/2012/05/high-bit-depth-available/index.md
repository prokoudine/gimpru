---
title: "Доступна обработка с высокой разрядностью на цветовой канал"
date: "2012-05-04"
---

Сегодня на конференции Libre Graphics Meeting в Вене мы официально объявили о том, что в нестабильной версии GIMP доступна обработка с точностью 16 и 32 разряда на цветовой канал.

Переключатель находится в меню «Изображение» ("Image"):

![Переключатель](http://i.minus.com/img2J9WQCzk4l.jpg)

В этом режиме работают инструменты трансформации, рисования и цветокоррекции, портированные на GEGL фильтры. Доступны загрузка и сохранение PNG в 16bit, сохранение EXR и HDR. Мы также улучшили работу с индексированными изображениями: теперь по ним можно пройтись хоть инструментом размазывания, хоть фильтрами.

Рассмотрим на примере. Берём файл OpenEXR, экспортированный из darktable. Он сразу открывается в режиме 32bit float.

![Оригинал](http://i.minus.com/i4ncS2v9uQodE.jpg)

Если назло себе перейти в 8-разрядный режим и в уровнях дёрнуть гамму до 2, на гистограмме получается «расчёска» из-за потери цветовых данных в результате ошибок округления.

![8bit](http://i.minus.com/iJ7WlMZ4GjufB.jpg)

Если назло себе ничего не делать и остаться в 32-разрядном режиме, никакой особой потери не происходит:

![32bit](http://i.minus.com/ibtoS7H6cXqNw6.jpg)

Окончательный набор функций и сроки готовности будут определены по мере дальнейшей работы. Пока что мы хотим доделать порт на GEGL и вылизать производительность.
