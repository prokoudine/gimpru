---
title: "Об управлении цветом в GIMP 2.10"
date: "2012-08-24"
---

Мы уже не раз сообщали, в версии 2.10 программа наконец-то сможет работать с данными в режимах 16 и 32 разряда на цветовой канал. Как это повлияет на управление цветом?

Пару недель назад Оэвинд опубликовал в списке рассылки [примерный план](https://mail.gnome.org/archives/gimp-developer-list/2012-August/msg00084.html) того, как GIMP 2.10 будет работать с изображениями в разных цветовых пространствах.

Начиная с точности в 16 разрядов на канал GIMP всегда работает с линейными данными. Таким образом при открытии файла модуль правления цветом будет выполнять преобразование в линейный RGBA.

В ходе работы каждый оператор GEGL может использовать предпочтительное цветовое пространство; в этом случае будет производиться конверсия с помощью библиотеки babl.

При экспорте или цветопробе модуль управления цветом будет выполнять преобразование в конечное пространство.

Как видите, ничего сверхестественного в этом нет: всё достаточно предсказуемо и логично.

Публикация плана связана с работой Элле Стоун, которая по собственной инициативе в июле взялась портировать модуль управления цветом на LittleCMS v2.

На текущий момент новый модуль уже использует LCMS2 и поддерживает преобразования с высокой разрядностью на цветовой канал (пока только 8-bit integer, 16-bit integer и 32-bit floating point). Самый последний отчёт с техническими подробностями доступен [на сайте Элле](http://ninedegreesbelow.com/temp/gimp-lcms-4.html).

Планов на версию 2.10 по реализации работы в произвольных цветовых пространствах и с произвольными цветовыми моделями (кроме субтрактивных) у нас пока нет. Тем не менее, основы работы с разными моделями внутри операторов GEGL уже заложены. В частности, портированный Максиме Никко генератор шума умеет работать не только в RGB, но и в CIE LCh.
