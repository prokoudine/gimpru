---
title: "Выпущен GIMP 2.8.2"
date: "2012-08-24"
---

Мы выпустили первое обновление стабильной версии GIMP 2.8 с массой важных исправлений. Нумерация версий также поменялась. Теперь микроверсия релизов бывает нечётной только для версий в Git. У официальных релизов номер всегда чётный (2.8.2, 2.8.4 и т.д.).

Вот самые важные изменения:

- при закрытии изображения теперь упоминается, было ли оно экспортировано;
- отрисовка стала работать быстрее при включённых фильтрах монитора, таких как управление цветом;
- возобновлено запоминание параметров экспорта в JPEG;
- для Windows снова включены нечаянно потерявшиеся параметры печати;
- подготовлено временное решение проблемы с показом размеров файлов в Windows;
- добавлена возможность собирать GIMP для OSX без X11.

[Ссылки для скачивания](http://gimp.ru/viewpage.php?page_id=8) обновлены. Митч постарается в ближайшее время сделать GIMP.app для Mac.
