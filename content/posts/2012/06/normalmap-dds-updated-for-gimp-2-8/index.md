---
title: "Расширения Normalmap и DDS обновлены для GIMP 2.8"
date: "2012-06-30"
---

Шон Кёрст стряхнул пыль с расширений Normalmap и DDS plug, которые не трогал уже больше двух лет. Оба расширения будут интересны тем, кто занимается 3D и графикой для компьютерных игр. В частности, Normalmap разрабатывается как клон аналогичного расширения NVidia для Photoshop.

![GIMP Normalmap plug-in, 2D view](http://i.minus.com/ibia5A5SbecyFT.png "GIMP Normalmap plug-in, 2D view")

В расширении Normalmap никаких изменений кроме добавленной совместимости с GIMP 2.8 не произошло. А вот с модулем для открытия и экспорта файлов Direct Draw Surface всё интереснее.

![DDS exporting options](http://i.minus.com/j9jhuBTYCnmDM.png "DDS exporting options")

Здесь не только совместимость с новой версии программы, но и поддержка сохранения существующей цепочки мипмапоа и даже улучшения в качестве компрессии DXT.

В разделе TODO Шон указал, что планирует переписать модуль на GEGL, так что можно будет работать с пиксельными форматами, поддерживающими большую глубину цвета.

Оба модуля доступны в виде исходного кода и сборок для Windows — как 32bit, так и 64bit:

- скачать [GIMP Normalmap](http://code.google.com/p/gimp-normalmap/downloads/list);
- скачать [GIMP DDS](http://code.google.com/p/gimp-dds/downloads/list).
