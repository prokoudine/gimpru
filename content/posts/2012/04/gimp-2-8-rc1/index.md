---
title: "GEGL обновился, первый релиз-кандидат GIMP 2.8 уже с нами"
date: "2012-04-04"
---

Стресс-тест пользователей GIMP на устойчивость победно шагает по планете: мы выпустили первый релиз-кандидат версии 2.8, не забыв обновить GEGL, куда попали разные полезные новшества.

Из-за необходимости поломать в GEGL совместимость по API и ABI нам пришлось отказаться от жестокой первоапрельской шутки с самым настоящим выпуском конечной версии 2.8. Так что теперь мы просто дадим релиз-кандидату поработать на компьютерах пользователей ещё недельку-другую. А то вдруг в коде завелось что-то полосатое, двадцатиногое и со щупальцами? Нам-то смешно, а у вас дети растут.

Что касается GEGL, самым главным новшеством там является всё-таки не поломка API/ABI, а использование GPU для рендеринга и вычислений. Мы так часто об этом писали, что теперь уже никак не можем остановиться.

Основы этой функциональности были заложены в 2009 году Джёрсоном Майклом Перпетуа (Jerson Michael Perpetua), который был нашим студентом GSoC. Два года спустя его работу, также в рамках GSoC, продолжил Виктор Оливейра (Victor Oliveira), который на основе этого кода подготовил базовую реализацию с использованием OpenCL.

Надо сказать, Виктору повезло немного больше: на его проект обратил внимание сотрудник AMD, который «пробил» оплачиваемую контрактную работу по доводке проекта до рабочего состояния, что и было проделано. Больше того, AMD выдали Виктору новую модную карточку для тестирования кода, но бразильская таможня, очевидно, была немного покусана русской, поэтому половина проекта была всё равно сделана на Nvidia.

Теперь GEGL умеет рендерить на стороне GPU и там же выполнять около десятка различных операций, включая уровни, виньетирование, размывание в движении, гауссово размывание и хипстерский c2g. Чтобы это работало, нужно запускать GIMP с переменной GEGL\_USE\_OPENCL=yes.

Рост производительности в сравнении с аналогами, работающими на CPU, разнится от операции к операции. Примитивненький box blur с огромным радиусом, к примеру, работает примерно в 70 раз быстрее. Но это с учётом того, что многопоточность в GEGL пока не починили.

К операциям на OpenCL добавились две обычные, без ускорения:

1. Перспективная трансформация. Написана в прошлом году Микаэлем Магнуссоном для переработанного инструмента коррекции перспективы.
2. Глобальный маттинг. Написана Яном Рюггом в начале этого года и нужна для реализации нового инструмента выделения объектов на фоне.

Наконец, Мишель Мюре довёл до логического завершения локализуемость GEGL. Начальный русский перевод уже готов и доступен в этом релизе.

![Локализованный c2g](http://i.minus.com/ibxL2CiDZOdWHN.png)

Остаётся упомянуть ветку GIMP под названием [goat-invasion](http://gimp.ru/news.php?readmore=183), в которой последние три недели происходит современный аналог химической свадьбы Христиана Розенкрейца. Старый менеджер тайлов заменён на код GEGL, основные инструменты цветокоррекции тоже переписаны на GEGL, а фильтры GIMP понемногу меняются на операции GEGL и доступны прямо из меню.

Всё это звучит очень вдохновляюще, но спринт закончен, и в обозримом будущем может произойти некоторый спад активности. В ваших силах показать, какие вы клёвые программисты на C, и сделать так, чтобы поток коммитов не иссякал.

Сборок для Windows следует ждать в самое ближайшее время (славься, [Парта](http://www.partha.com/)!).

Из прочих новостей невозможно не отметить досрочный блиц-курс по юзабилити, который Петер Сиккинг каждый год проводит в Австрии для местных студентов-эргономистов. В этом году Петер, рулящий в проекте юзабилити и UX, избрал для проработки не скажем какой замечательный плагин к программе, который заслуживает быть интерактивным инструментом. Сдача документации по этому проекту состоится 11 апреля, после этого и поговорим :)

Ах, да. Йон Нордби планирует в обозримом будущем обновить свои [привязки GEGL к GTK+ и Qt](http://linuxgraphics.ru/news.php?readmore=1839), поэтому ежели вам не терпится, положим, написать какие-нибудь объективные тесты производительности OpenCL, ждать осталось недолго. Мы искренне в это верим.
