---
title: "Мы снова в Google Summer of Code"
date: "2012-03-17"
---

Как сегодня стало известно, нас снова приняли в Google Summer of Code. Это ежегодная программа компании Google, в рамках которой студенты получают возможность поработать над любимой программой за деньги.

Часть [идей возможных проектов](http://wiki.gimp.org/index.php/Hacking:GSOC/Archive#Projects_in_GSoC_2012) перетекла из прошлогоднего списка. В качестве новой идеи добавлен ранее запланированный объединённый инструмент трансформации, над которым в прошлом году [начал работать](http://gimp.ru/news.php?readmore=162) Микаэль Магнуссон. Собственно, он и планирует заняться этим проектом, так что если ничто ему не помешает, вопрос можно считать решённым.

Ещё один возможный проект — превращение Liquid Rescale в интерактивный инструмент. В прошлом году Карло Бальдасси начал переделывать свой популярнейший фильтр в инструмент на основе GEGL, но работа заглохла. Буквально позавчера редакции GIMP.ru практически удалось убедить Петера взять эту идею для отработки на студентах во время очередного курса юзабилити. Последнее слово за Митчем.

Как вы понимаете, в связи с неожиданным [нашествием пятиногой козы](http://gimp.ru/news.php?readmore=183) предпочтение будет отдаваться проектам, которые посвящены переносу инструментов и фильтров на GEGL. Мы стараемся по возможности упростить эту задачу. Буквально сегодня Оэвинд добавил в babl поддержку форматов, основанных на фиксированных палитрах цветов.

Подача заявок от студентов начнётся через неделю, а список принятых проектов станет известен лишь через месяц. Набираемся терпения и скрещиваем пальчики.
