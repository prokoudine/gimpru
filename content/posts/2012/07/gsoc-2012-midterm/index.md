---
title: "Все студенты прошли промежуточную оценку в GSoC2012"
date: "2012-07-25"
---

Все пятеро студентов, с которыми мы работаем по программе Google Summer of Code 2012, успешно прошли июльскую промежуточную оценку работы.

Код доступен в следующих ветках Git:

- объединённый инструмент трансформации: [soc-2012-unified-transformation](http://git.gnome.org/browse/gimp/log/?h=soc-2012-unified-transformation)
- редактор нод на GEGL: [soc-2012-editor](http://git.gnome.org/browse/gegl/log/?h=soc-2012-editor)
- порты фильтров GIMP на операции GEGL: [soc-2012-ops](http://git.gnome.org/browse/gegl/log/?h=soc-2012-ops)
- разнообразная работа, связанная с портированием GIMP на GEGL: [soc-2012-ville](http://git.gnome.org/browse/gegl/log/?h=soc-2012-ville), также в ветке master.

Новый инструмент, по-хорошему, надо показывать на видео. Давайте пока обойдёмся скриншотом.

![Объединённый инструмент трансформации](http://i.minus.com/i3tRt1yBF7zYW.jpg "Объединённый инструмент трансформации")

Напомним, что это пока половина работы. Перемещение слоёв сейчас работает с ошибками, масштабирование тоже не безгрешно, а числовой ввод параметров не только не реализован, но даже ещё не учтён в спецификации. Зато есть работающая галка "From center", которую позже переименуют в "From pivot".

Работу над этим инструментом Микаэль Магнуссон [начал ещё в прошлом году](http://gimp.ru/news.php?readmore=162), заинтересовавшись первой версией спецификации, написанной Петером Сиккингом. Программа GSoC дала ему финансирование, необходимое для завершения проекта.

Что касается редактора нод, он пока что крайне ограниченно пригоден к работе: предпросмотр работает только для таких форматов как JPEG и PNG, на нодах режимов смешивания он падает, микшировать выходы нескольких нод ещё не умеет. Айзек, тем не менее, настроен достаточно решительно.
