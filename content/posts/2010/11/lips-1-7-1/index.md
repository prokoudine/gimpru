---
title: "ЛИПС 1.7.1"
date: "2010-11-18"
---

Выпущено обновление сценария ЛИПС 1.7.1 (лаборатория имитации пленочных снимков).

Изменения в новой версии:

- совместимость с GIMP 2.7.2;
- процессы «Ч/Б» и «Сепия» объеденены в единый процесс «Монохром»;
- добавлен новый процесс «драм-зерно».
- удалён процесс «Простая зернистость».
- практически полностью переделан процесс «Ломо».

Релиз был задержан почти на неделю в связи с обнаруженными критическими ошибками.

[Описание и ссылки на закачивание](http://gimp.ru/viewpage.php?page_id=28)

[Ссылка на английский релиз](http://registry.gimp.org/node/24639)
