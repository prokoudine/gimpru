---
title: "Новый инструмент: Cage transform"
date: "2010-11-05"
---

Вчера вечером в основную ветку разработки GIMP был влит код нового инструмента Cage transform, написанного в рамках программы Google Summer of Code. Этот инструмент позволяет выполнять выборочные преобразования объектов прямо на холсте.

Работа инструмента основана на алгоритме, описанном в научной работе [Green Coordinates](http://www.math.tau.ac.il/~lipmanya/GC/gc.htm), опубликованной на SIGGRAPH 2008. Научная работа была написана больше с прицелом на трёхмерное моделирование, но общий принцип оказался применим и к двухмерной графике. Вот видеодемонстрация, [опубликованная](http://www.gimpusers.com/news/00333-first-look-at-the-cage-based-transform-tool) на gimpusers.com всего несколько дней назад:

<iframe src="//www.youtube.com/embed/YLt3Uo5cMbA" height="322" width="572" allowfullscreen frameborder="0"></iframe>

Внимания заслуживают отдельные, несколько технические моменты реализации инструмента.

Во-первых, это первый _настоящий_ инструмент GIMP, использующий [GEGL](http://www.gegl.org/) — новый движок GIMP для неразрушающей обработки графики. Для этого была написана новая операция GEGL, выполняющая все геометрические преобразования. Так что это не просто новый инструмент, но и ещё один шаг в сторону полноценного использования GEGL в программе.

Во-вторых, эта новая операция GEGL реализует примерно половину функциональности, необходимой для создания полностью интерактивной версии фильтра «Интерактивные искажения» (IWarp). Это означает, что момент появления работающего прямо на холсте аналога фильтра _Liquify_ из Photoshop существенно приблизился. Собственно говоря, Барак Иткин сейчас как раз тренируется на кошкахпытается портировать на GEGL фильтр «Вихрь и щипок» (Whirl and pinch), где выполняется схожая деформация объектов.

Новый инструмент GIMP и операция GEGL были написаны студентом GSoC Мишелем Мюре (Michael Muré) и доработаны Алексией. Для того, чтобы инструмент совершенно точно попал в версию 2.8, его нужно немного доработать и как следует протестировать. Но волноваться здесь особенно не о чем: разработчики всерьёз настроены сделать новый инструмент доступным пользователям как можно быстрее.

Если вы хотите самостоятельно протестировать инструмент Cage transform (кстати, объявляется конкурс на лучший вариант перевода), вам понадобятся GEGL и GIMP из Git (причём именно основная ветка, т.е. master). Выпуск новой версии GEGL, в которую уже внесены все необходимые изменения, задерживается из-за нерешённых инфраструктурных проблем.

Тем временем Митч вносит кое-какие важные изменения в интерфейс программы; подробнее об этом мы расскажем чуть позже.
