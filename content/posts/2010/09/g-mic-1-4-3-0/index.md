---
title: "G'MIC 1.4.3.0"
date: "2010-09-27"
---

Давид Шумперле выпустил новую версию метафильтра [G'MIC](http://gmic.eu/gimp.shtml). Предыдущую версию мы не анонсировали, так что список измений будет общим.

В принципе, он невелик:

- новые фильтры:
    - Spectral handling > Fourier watermark;
    - Degradations > Visible watermark;
- исправления ошибок;
- ускорение работы фильтров.

[Скачать](http://gimp.ru/viewpage.php?page_id=13)
