---
title: "Get Curves 20100831"
date: "2010-09-02"
---

Эль Самуко написал ещё одно интересное расширение, которое называется [Get Curves](https://sites.google.com/site/elsamuko/gimp/get-curves). Оно анализирует пару изображений, до и после обработки, и вычисляет кривую RGB, которая превращает изображение до обработки в изображение после обработки :)

Пока что у Get Curves даже нет графического интерфейса, а есть лишь пункт меню (_Colors/Цвет > Get RGB Curves_). Для работы нужно два изображения поместить в одно, причём изменённая версия должна быть в верхнем слое. Файл кривой будет автоматически сохранён в текущий каталог.

Для примера можно [скачать](https://sites.google.com/site/elsamuko/gimp/get-curves/elsamuko-kodachrome?attredirects=0&d=1) готовую кривую Kodachrome, полученную таким способом из пары картинок на странице [http://homepage.mac.com/pbize1/Scripts/PSAK200.html](https://sites.google.com/site/elsamuko/gimp/get-curves).

Установка в Linux выполняется командой

$ gimptool-2.0 --install elsamuko-get-curves.c

Сборки для Windows пока нет (да и рано ещё, наверное).
