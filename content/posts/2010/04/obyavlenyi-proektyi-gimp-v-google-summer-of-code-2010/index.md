---
title: "Объявлены проекты GIMP в Google Summer of Code 2010"
date: "2010-04-27"
---

Стали известны проекты GIMP/GEGL в ежегодной программе Google Summer of Code, в рамках которой студенты получают финансирование за работу над выбранным проектом в любимой свободной программе.

Итак, в этом году три проекта:

- три оператора отображения тонов для GEGL и оператор разбора изображения на передний и задний план (matting);
- инструмент трансформации на основе научной работы ["Green Coordinates"](http://www.math.tau.ac.il/~lipmanya/GC/gc_techrep.pdf), опубликованной на SIGGRAPH в 2008 году; инструмент позволит выборочно искажать и перемещать части изображения (да-да, это функционально пересекается с новым «кукольным» инструментом в Adobe Photoshop CS5);
- реализация инфраструктуры для показа параметров инструментов на холсте; при условии успешной реализации проекта вам придётся меньше обращаться к панели параметров текущего инструмента.

В прошлом GIMP достаточно успешно участвовал в программе, хотя пока не все проекты доступны конечному пользователю.
