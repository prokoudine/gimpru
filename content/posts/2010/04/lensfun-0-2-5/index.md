---
title: "LensFun 0.2.5"
date: "2010-04-01"
---

Выпущена новая версия библиотеки [LensFun](http://lensfun.sourceforge.net/), которая предназначена для коррекции искажений, вносимых фотооптикой и используется в UFRaw. Пользователям Windows достаточно скачать актуальную сборку UFRaw.

Изменения:

- как обычно, добавлено много новых фотоаппаратов:
    - Canon EOS-1D Mark II, EOS-1Ds Mark III, EOS-1D Mark III, EOS Digital Rebel XTi, EOS Digital Rebel XS, EOS Digital Rebel XSi, EOS REBEL T1i, EOS D2000C, EOS-1D Mark IV, PowerShot G9, G11, Pro70, SX1 IS, S90, D2000C, DIGITAL IXUS 80 IS;
    - Casio EX-FH20;
    - Hasselblad 500 mech., H3D;
    - Kodak DCS520C, DC120 ZOOM Digital Camera, Kodak Digital Science DC50 Zoom Camera;
    - Leica Digilux 3 и 4;
    - NIKON D3S, COOLPIX P6000;
    - Olympus EP-1, E-30, SP350, SP500UZ, SP560UZ;
    - Panasonic D-LUX 3LX3, DMC-FX150, DMC-GH1, DMC-G1, DMC-L1, DMC-L10, DMC-LX2, DMC-FZ18, DMC-FZ35, DMC-FZ50, DMC-FZ8;
    - Pentax K2000, K200D, K-x;
    - Samsung GX-1S;
    - SIGMA DP1S, DP2, DP1X, DP2S и SD15;
- и объективов:
    - Canon EF-S 17-55mm f/2.8 IS USM;
    - Canon 28mm f/1.8 USM;
    - Canon 135mm f/2.0L USM;
    - Canon 70-200mm f/4L IS USM;
    - Nikkor 18-135mm f/3.5-5.6G IF-ED AF-S DX;
    - Pentax 12-24 f/4.0 DA;
    - Sigma 50mm f/1.4 EX DG HSM;
    - Sigma 50mm f/1.4 с байонетом на Nikon F AF
- поддержка кроссплатформенной сборки с помощью cross-mingw32 (см. README).
- исправлены различные ошибки, в том числе связанные с аллокацией памяти;
- добавлен новый алгоритм коррекции хроматических аберраций LF\_TCA\_MODEL\_POLY3 ("poly3"), совпадающий с алгоритмом в Hugin, но использующий более высокий порядок (r^4);
- исправлена модель LF\_TCA\_MODEL\_LINEAR;
- написано [новое руководство](http://lensfun.sourceforge.net/calibration-tutorial/lens-calibration.html) по калибровке объективов при помощи Hugin;
- поддержка инструкций SSE и SSE2 (Klaus Post, команда Rawstudio).

[Скачать](http://sourceforge.net/projects/lensfun/files/0.2.5/lensfun-0.2.5.tar.bz2/download)
