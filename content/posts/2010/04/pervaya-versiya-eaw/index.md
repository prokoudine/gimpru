---
title: "Первая версия EAW"
date: "2010-04-06"
---

Автор свободного Raw-конвертера dlraw Михаэль Мунцерт написал [расширение EAW](http://www.mm-log.com/blog/2010-04-05/gimp-edge-avoiding-wavelets), реализующее технологию избегающих краёв вейвелетов (edge avoiding wavelets), уже известную некоторым по модулю «Эквалайзер» в [darktable](http://darktable.sourceforge.net/).
