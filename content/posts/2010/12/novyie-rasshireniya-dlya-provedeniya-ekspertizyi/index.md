---
title: "Новые расширения для проведения экспертизы"
date: "2010-12-27"
---

El Samuko выпустил ещё два расширения для проведения экспертизы на предмет внесения изменений в изображения.

[Первое](https://sites.google.com/site/elsamuko/forensics/hsv-analysis) выполняет анализ в цветовом пространстве HSV, [второе](https://sites.google.com/site/elsamuko/forensics/lab-analysis) — в LAB. В качестве примера автор взял известную фотографию, сделанную British Petroleum, на которой цвет воды Мексиканского залива скорректирован и сделан более синим.

![Equipment already in use cleaning the beaches of Orange Beach](http://farm5.static.flickr.com/4145/4834228009_d6c27bf6c2.jpg)

К примеру, смотрим на диаграмму LAB и видим, что синие тона доходят до самого края диапазона, что свидетельствует  о растягивании тонов:

![Диаграмма LAB](https://sites.google.com/site/elsamuko/_/rsrc/1293383873402/forensics/lab-analysis/BP_LAB.png?height=320&width=320 "Диаграмма LAB")

Для сравнения вот диаграмма всего пространства:

![Всё диаграмма](https://sites.google.com/site/elsamuko/_/rsrc/1293384021821/forensics/lab-analysis/FULL_LAB.png?height=320&width=320 "Всё диаграмма")

Скачать анализатор в HSV:

- [исходный код](https://sites.google.com/site/elsamuko/forensics/hsv-analysis/elsamuko-hsv-analysis.c?attredirects=0&d=1);
- [сборка для Mac](https://sites.google.com/site/elsamuko/forensics/hsv-analysis/elsamuko-hsv-analysis.mac.zip?attredirects=0&d=1).

Скачать анализатор в LAB:

- [исходный код](https://sites.google.com/site/elsamuko/forensics/lab-analysis/elsamuko-lab-analysis.tar.gz?attredirects=0&d=1);
- [сборка для Mac](https://sites.google.com/site/elsamuko/forensics/lab-analysis/elsamuko-lab-analysis.mac.zip?attredirects=0&d=1).
