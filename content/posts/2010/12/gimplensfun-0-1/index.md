---
title: "GimpLensfun 0.1"
date: "2010-12-03"
---

Выпущена первая версия расширения [GimpLensfun](http://seebk.github.io/GIMP-Lensfun/), которое упрощает коррекцию оптических дисторсий в GIMP при помощи библитеки [LensFun](http://lensfun.sourceforge.net/), созданной Андреем Заболотным.

![GimpLensfun](http://gimp.ru/images/news/plugins/gimp-lensfun/gimp-lensfun-010.png "GimpLensfun")

У фильтра очень простой интерфейс, где выбирается, в порядке очерёдности, производитель камеры, модель камеры, и объектив. Фокусное расстояние по возможности само подставляется из Exif.

![](/images/news/plugins/gimp-lensfun/DSCF0142.JPG)

Фильтр пока что не учитывает другие возможности LensFun, такие как коррекция хроматических аберраций и виньетирования.

В настоящее время доступен только исходный код.

[Скачать](http://seebk.github.io/GIMP-Lensfun/)
