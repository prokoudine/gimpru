---
title: "Дайджест №44 (22-28 ноября 2010)"
date: "2010-11-29"
---

Изменений на предыдущей неделе снова было не очень много, и все они снова относятся к доработке реализованного ранее.

Основная работа была выполнена Митчем. Он подчистил код редактора динамики рисования и улучшил вид кнопки сброса размера кисти. Кроме того, Митч убрал переключатель списка/сетки в раскрывающемся списке различных ресурсов (кистей, градиентов и пр.).

Помимо внесения улучшений исправлены различные ошибки. Например, при копировании контура из документа в документ этот контур больше не масштабируется до размера изображения. Заодно исправлены некоторые недочёты в коде нового редактора текстовых слоёв.
