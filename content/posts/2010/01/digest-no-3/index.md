---
title: "Дайджест №3 (1-15 января 2010)"
date: "2010-01-17"
---

Очередной дайджест немногословен, но содержит самые настоящие откровения :)

Изменения затрагивают только GEGL и GIMP. В babl изменений не было, зато выпущена версия 0.1.2. О ней мы напишем подробнее как только выйдет новая версия GEGL.

GIMP:

- продолжена работа над однооконным режимом;
- на примере диалога с параметрами экспорта в PNG начат эксперимент с созданием интерфейса при помощи Glade и GtkBuilder;
- исправлена ошибка при сохранении индексированных PPM;
- исправлено мерцание по краям при навигации по нажатой средней клавише мыши;
- впервые за бог весть какое время в devel-docs добавлен документ, содержащий таблицу трудозатрат, по которой можно примерно оценить сроки готовности 2.8 (ориентировочно, декабрь 2010).

GEGL:

- дефис в качестве разделителя слов в названиях операторов;
- улучшена и исправлена система тестов регрессии в композиции XML;
- обновлены тесты композиции графов.
