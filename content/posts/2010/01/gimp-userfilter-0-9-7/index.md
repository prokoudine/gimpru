---
title: "GIMP UserFilter 0.9.7"
date: "2010-01-01"
---

Торстен Нойер выпустил новую версию [GIMP UserFilter](http://gimpuserfilter.sourceforge.net/).

Изменения:

- исправлены несущественные ошибки;
- добавлен контроль размера шага для параметров фильтров;
- добавлена возможность выбирать способ обработки исходных пикселов вне изображения;
- улучшен оптимизатор байт-кода.

[Скачать](http://sourceforge.net/projects/gimpuserfilter/files/gimpuserfilter/gimp-plugin-userfilter-0.9.7/)
