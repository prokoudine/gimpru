---
title: "GIMP участвует в Open Source Awards 2010"
date: "2010-10-15"
---

Между прочим, по итогам предварительного голосования GIMP стал финалистом ежегодного конкурса [Open Source Awards 2010](http://www.packtpub.com/open-source-awards-home). В номинации Open Source Graphics Software он соседствует с проектами Inkscape, Scribus, Blender и jmonkeyengine.

[![Голосуйте за GIMP](http://gimp.ru/images/news/events/open-source-awards-2010/gimp.png "Голосуйте за GIMP")](https://www.packtpub.com/open-source-awards-home/vote-open-source-graphics-software)

Что можно сделать, чтобы GIMP из финалиста превратился в победителя и получил главный приз ($2500)? [Проголосовать](https://www.packtpub.com/open-source-awards-home/vote-open-source-graphics-software) за проект до 5 ноября. Издательство Packt Publishing, которое организует этот конкурс, объявит победителей на неделе, начинающейся 15 ноября.

Думаете, всё так просто? Да если бы :) Решение по распределению трёх первых мест на основании голосов сообщества примут [четыре судьи](http://www.packtpub.com/open-source-awards-home/open-source-graphic-software-judges):

1. Jacob Gube, основатель и главный редактор журнала для веб-разработчиков и дизайнеров [Six Revisions](http://sixrevisions.com/), а также редактор электронного журнала для дизайнеров и художников.
2. Mauricio Duque, графический дизайнер с десятилетним опытом работы в различных IT-компаниях, основатель проектов [snap2objects](http://www.snap2objects.com/) и [vision-futuro.com](http://www.vision-futuro.com/).
3. Anne-Mieke Bovelett, профессиональный дизайнер и владелец собственного [агентства](http://www.gepresenteerd.nl/) в Нидерландах, специализирующегося на мультимедии и маркетинговых услугах.
4. Jan Cavan, веб-дизайнер и иллюстратор, успешно поучаствовала в создании многих книг и журналов, не говоря о сетевых публикациях. В настоящее время она работает проектировщиком интерфейсов в [SendGrid](http://sendgrid.com/) и выполняет частные заказы под вывеской проекта [Dawghouse Design Studio](http://www.dawghousedesignstudio.com/).

Судьи будут оценивать:

- производительность приложений;
- размер сообщества и поддержку проекта сообществом (голосование);
- использование приложений в коммерческих проектах;
- эргономичность интерфейсов;
- расширяемость приложений.

Вот так-то. Но вы не отчаивайтесь :)
