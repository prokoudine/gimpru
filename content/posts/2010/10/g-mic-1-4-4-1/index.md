---
title: "G'MIC 1.4.4.1"
date: "2010-10-12"
---

Давид продолжает активно выпускать новые версии метафильтра G'MIC, превращая новостную ленту GIMP.ru в новостную ленту [gmic.sourceforge.net](http://gmic.eu/gimp.shtml).

Кумулятивный список изменений версий 1.4.3.1, 1.4.4.0 и 1.4.4.1 таков:

Новый фильтр Rendering/Snowflake:

[![Rendering/Snowflake](http://farm5.static.flickr.com/4086/5051550028_e777e4204f_m.jpg "Rendering/Snowflake")](http://farm5.static.flickr.com/4086/5051550028_e777e4204f_b_d.jpg)

Новый фильтр Rendering/Rainbow:

[![Rendering/Rainbow](http://farm5.static.flickr.com/4086/5051550022_28c6e3819e_m.jpg "Rendering/Rainbow")](http://farm5.static.flickr.com/4086/5051550022_28c6e3819e_b_d.jpg)

Новый фильтр Contours/Thin Edges:

[![Contours/Thin Edges](http://farm5.static.flickr.com/4085/5071305778_b2cbbd3fc1_m.jpg "Contours/Thin Edges")](http://farm5.static.flickr.com/4085/5071305778_b2cbbd3fc1_b_d.jpg)

Новый фильтр Contours/Edge offsets:

[![Contours/Edge offsets](http://farm5.static.flickr.com/4124/5071305784_eecd78fab9_m.jpg "Contours/Edge offsets")](http://farm5.static.flickr.com/4124/5071305784_eecd78fab9_b_d.jpg)

Новый фильтр Rendering/3d extrusion:

[![Rendering/3D extrusion](http://farm5.static.flickr.com/4146/5071305794_c1153439e6_m.jpg "Rendering/3D extrusion")](http://farm5.static.flickr.com/4146/5071305794_c1153439e6_z_d.jpg)

Новый фильтр Rendering/3d lathing:

[![Rendering/3d lathing](http://farm5.static.flickr.com/4083/5056233519_ea77b9d486_m.jpg "Rendering/3d lathing")](http://farm5.static.flickr.com/4133/5056850296_75497f122d_b_d.jpg)

Новый фильтр Contours/Skeleton:

[![Contours/Skeleton](http://farm5.static.flickr.com/4129/5062497872_3f1595508c_m.jpg "Contours/Skeleton")](http://farm5.static.flickr.com/4129/5062497872_3f1595508c_b_d.jpg)

Улучшены возможности 3D-рендеринга: теперь можно указывать положение источника света и угол поля зрения.

Исправлены ошибки в движке G'MIC.
