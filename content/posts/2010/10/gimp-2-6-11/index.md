---
title: "GIMP 2.6.11"
date: "2010-10-05"
---

Выпущено обновление стабильной версии [GIMP](http://www.gimp.org/) с исправлением ошибок.

Перечень исправленных ошибок:

- ошибка при печати и предпросмотре печати при использовании Cairo 1.10 ([#631199](https://bugzilla.gnome.org/show_bug.cgi?id=631199));
- некорректная работа функций gimp-parasite-attach и gimp-image-parasite-attach, приводившая к падению script-fu ([#572865](https://bugzilla.gnome.org/show_bug.cgi?id=572865));
- ошибка в функциях string-append и gimp-drawable-get-name при работе с некоторыми символами ([#628893](https://bugzilla.gnome.org/show_bug.cgi?id=628893));
- фильтр «Ретинекс» создавал гало ([#623850](https://bugzilla.gnome.org/show_bug.cgi?id=623850));
- неправильные значения в документации к фильтру «Край» ([#624487](https://bugzilla.gnome.org/show_bug.cgi?id=624487));
- фильтр «Разница по Гауссу» создавала пустой документ, если выбрано инвертирование ([#557380](https://bugzilla.gnome.org/show_bug.cgi?id=557380));
- фильтр типов изображений не включал .rgba SGI ([#627009](https://bugzilla.gnome.org/show_bug.cgi?id=627009));
- при запуске импорта PostScript открывалось окно консоли ([#626020](https://bugzilla.gnome.org/show_bug.cgi?id=626020));
- некорректные значения альфа-канала в текстурах Wood 1 и Wood 2 ([#624698](https://bugzilla.gnome.org/show_bug.cgi?id=624698));
- сохранённое из Google Docs создавало сообщение об ошибке 'gimp-image-set-resolution' ([#624275](https://bugzilla.gnome.org/show_bug.cgi?id=624275)).

Обновлённые локализации: немецкая, испанская, итальянская, японская, румынская, китайские (Гонконг, Тайвань).

[Ссылки на скачивание](http://gimp.ru/viewpage.php?page_id=8) обновлены. Сборки для Mac OS X пока не готовы и будут объявлены отдельно.
