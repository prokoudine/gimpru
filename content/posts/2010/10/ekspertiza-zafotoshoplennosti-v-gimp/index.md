---
title: "Экспертиза зафотошопленности в GIMP"
date: "2010-10-15"
---

Солнцеликий El Samuko в очередной раз доказал, что является одним из самых талантливых авторов дополнений к GIMP, опубликовав [два расширения](https://sites.google.com/site/elsamuko/forensics), помогающих в экспертизе изображений на предмет «фотошопа».

Первое из них, [clone detection](https://sites.google.com/site/elsamuko/forensics/clone-detection) («_Изображение > Copy Move»_) находит и показывает образцы, взятые штампом для клонирования, и обработанные этими образцами участки.

Второе, [error level analysis](https://sites.google.com/site/elsamuko/forensics/ela) (_«Изображение > Error Level Analysis»_), реализованное как сценарий на Script-Fu, выделяет наиболее вероятно обработанные или даже искусственно добавленные участки изображения при помощи [анализа уровня ошибок](http://errorlevelanalysis.com/).

Скачать детектор штампа:

- [исходный код](https://sites.google.com/site/elsamuko/forensics/clone-detection/elsamuko-copy-move.c);
- [сборка для Windows.](https://sites.google.com/site/elsamuko/forensics/clone-detection/elsamuko-copy-move-w32.zip?attredirects=0&d=1)

[Скачать](https://sites.google.com/site/elsamuko/forensics/ela/elsamuko-error-level-analysis.scm?attredirects=0&d=1) анализатор уровня ошибок.
