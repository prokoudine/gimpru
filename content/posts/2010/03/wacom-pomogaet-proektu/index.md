---
title: "Wacom помогает проекту"
date: "2010-03-10"
---

Сотрудник компании Wacom, занимающийся разработкой[ драйверов Wacom для Linux](http://linuxwacom.sourceforge.net/), помог Алексии получить модель Intuos с дополнительными стилусами.

Вчера планшет наконец до неё доехал. В тот же вечер:

`commit 5fda650281eec30f577cbfffecb394a2b0d8e082 Author: Alexia Death <> Date: Tue Mar 9 22:22:38 2010 +0200 app: Support for wheel input found in highend wacom tablets&tools`
