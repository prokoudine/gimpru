---
title: "G'MIC 1.3.3.5"
date: "2010-02-01"
---

Выпущена новая версия расширения [G'MIC](http://gmic.sourceforge.net/index.shtml). Исправлены некоторые ошибки, выполнена оптимизация движка.

[Скачать](http://gmic.eu/gimp.shtml)
