---
title: "«Как тигра полосат»"
date: "2010-08-04"
---

В наступившем летнем затишье авторы проекта GIMP on OSX заодно обновили сборку текущей стабильной версии (2.6.10) и для Mac OS X 10.4 (Tiger).

[Скачать](http://gimp.ru/download/gimp/)
