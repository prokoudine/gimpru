---
title: "Выпущен GIMP 2.6.9"
date: "2010-06-23"
---

Выпущена новая _стабильная_ версия GIMP. Как обычно в ней только исправления ошибок и обновлённые переводы.

Исправлены следующие ошибки:

- диалог выбора шрифта остаётся видимым ([612618](https://bugzilla.gnome.org/show_bug.cgi?id=612618));
- двойное упоминание image/x-psd среди типов MIME в gimp.desktop ([622234](https://bugzilla.gnome.org/show_bug.cgi?id=622234));
- непортируемая конструкция test(1) в сценарии настройки сборки ([622196](https://bugzilla.gnome.org/show_bug.cgi?id=622196));
- не вполне корректное описание процедуры "histogram" ([620604](https://bugzilla.gnome.org/show_bug.cgi?id=620604));
- некорректное сохранение и загрузка параметров инструментов ([541586](https://bugzilla.gnome.org/show_bug.cgi?id=541586))
- разлетающийся в ширину диалог импорта документов PDF со слишком длинными названиями ([614153](https://bugzilla.gnome.org/show_bug.cgi?id=614153));
- падение выборочного гауссова размывания в Windows ([600112](https://bugzilla.gnome.org/show_bug.cgi?id=600112));
- диалог параметров сохранения в BMP игнорирует изменения, внесённые не мышью ([599233](https://bugzilla.gnome.org/show_bug.cgi?id=599233));
    
- инструмент ввода текста падает при попытке отредактировать текст в файле XCF, созданном версией 2.4.2 ([565001](https://bugzilla.gnome.org/show_bug.cgi?id=565001));
- миниатюра слоя внезапно прекращает обновляться ([610478](https://bugzilla.gnome.org/show_bug.cgi?id=610478));
- разделяемая память не высвобождается при завершении работы с программой ([609026](https://bugzilla.gnome.org/show_bug.cgi?id=609026));
- не работает экспорт в Alias PIX ([609056](https://bugzilla.gnome.org/show_bug.cgi?id=609056))
- несколько сообщений в диалоге параметров сохранения Raw показываются на английском несмотря на переведённость ([608188](https://bugzilla.gnome.org/show_bug.cgi?id=608188));
    
- операция GELG "path" обрушивает программу ([604820](https://bugzilla.gnome.org/show_bug.cgi?id=604820));
- падение при использовании инструмента рисования контуров ([603711](https://bugzilla.gnome.org/show_bug.cgi?id=603711));
- GIMP не собирается с libpng 1.4.0 ([607242](https://bugzilla.gnome.org/show_bug.cgi?id=607242));
- не работает сохранение в .ppm в индексированном режиме ([606372](https://bugzilla.gnome.org/show_bug.cgi?id=606372));
- сообщение "Сглаживание..." всегда показывается на английском языке ([605237](https://bugzilla.gnome.org/show_bug.cgi?id=605237));
    
- функция создания нового слоя из всего видимого должна использовать обновлённую проекциию ([604508](https://bugzilla.gnome.org/show_bug.cgi?id=604508)).

Кроме того, обновлены локализации, в том числе русская и украинская.

[Исходный код](http://ftp.gimp.org/pub/gimp/v2.6/gimp-2.6.12.tar.bz2) | [Windows 32bit](http://gimp.ru/download/gimp/) | [Windows 64bit](http://gimp.ru/download/gimp/)

Сборок для Mac пока нет, о их появлении объявим отдельно.
