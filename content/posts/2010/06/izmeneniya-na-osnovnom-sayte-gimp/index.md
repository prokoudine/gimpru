---
title: "Изменения на основном сайте GIMP"
date: "2010-06-24"
---

Нормальная работа [основного сайта GIMP](http://www.gimp.org/) восстановлена. Все данные перемещены на сервер gtk.org.

Кроме того, как и планировалось, Якуб Штайнер понемногу начал работать над новой статической версией на HTML5 (ветка [static-html5](http://git.gnome.org/browse/gimp-web/log/?h=static-html5) модуля gimp-web в Git).
