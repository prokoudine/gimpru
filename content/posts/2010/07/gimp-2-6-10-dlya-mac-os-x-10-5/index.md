---
title: "Появилась сборка GIMP 2.6.10 для Mac OS X 10.5"
date: "2010-07-31"
---

Появилась сборка текущей стабильной версии GIMP со всеми исправлениями для Mac OS X 10.5 (Leopard). Кроме того, обновлена уже существовавшая сборка этой версии для Mac OS X 10.6 (Snow Leopard).

[Скачать сборку для Leopard](http://gimp.ru/download/gimp/) | [Скачать сборку для Snow Leopard](http://gimp.ru/download/gimp/)
