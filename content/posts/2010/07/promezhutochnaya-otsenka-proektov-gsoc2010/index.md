---
title: "Промежуточная оценка проектов GSoC2010"
date: "2010-07-20"
---

Не без удовлетворения сообщаем, что на прошлой неделе все три студента объединённой организации GIMP и GEGL прошли промежуточную оценку результатов.

**Проект номер один** — реализация операторов отображения тонов (tone mapping) и оператора маттинга в GEGL. Дэнни Робсона можно по праву считать самым отличившимся студентом этого года. Для начала он портировал в GEGL три наиболее популярных оператора отображения тонов: fattal02, reinhard05 и mantiuk06. Они автоматически доступны через экспериментальный инструмент _Операции GEGL_ любому, собравшему код из Git. Ну и, конечно же, можно писать композиции на XML.

![mantiuk06.png](http://gimp.ru/images/news/gsoc2010-midterm/mantiuk06.png "Оператор отображения тонов mantiuk06 в инструменте применения операций GEGL")

Стоит предупредить, однако, что mantiuk06 является наиболее ресурсоёмким оператором отображения тонов, к которому без многопоточной обработки лучше и не подступаться. А в GEGL, между тем, многопоточная обработка по умолчанию отключена из-за пока что неисправленной взаимной блокировки процессов.

Кроме того, Дэнни написал простой инструмент объединения вилки экспозиции в HDR и специально по такому случаю реализовал загрузку и сохранение RGBE в GEGL. Скомпилированный двоичный файл инструмента находится в каталоге _tools_ и называется _exp\_combine_. Синтаксис очень прост:

$ exp\_combine конечный\_файл.hdr экспозиция\_1.jpg экспозиция\_2.jpg экспозиция\_3.jpg

Пока что инструмент очень капризен, и получить правильно собранный HDR-снимок сложновато. Кроме того, он совершенно бесхитростно предполагает, что снимки на входе заведомо выровнены относительно друг друга. Ничего ужасного в этом нет — при наличии основы дополнительный функционал всегда можно дописать, тем более что с задачей выравнивания хорошо справляется _align\_image\_stack_ из hugin, используемый в [Luminance HDR](http://qtpfsgui.sourceforge.net/) (Qtpfsgui).

Сейчас Дэнни работает над третьей частью проекта — оператором маттинга, т.е. извлечения объекта переднего плана из фона. В качестве алгоритма выбран метод[Левина-Лишинского-Вайсса](http://www.wisdom.weizmann.ac.il/~levina/papers/Matting-Levin-Lischinski-Weiss-CVPR06.pdf). К этой части работы Дэнни приступил совсем недавно, но кое-какой прогресс уже есть.

**Проект номер два** — общая инфраструктура для рисования параметров инструментов на холсте. Это не совсем удачный проект. Дело в том, что команда переоценила готовность GTK+ и GIMP к нему. Те, кто следит за нашими еженедельными дайджестами, помнят, что в июне Митч Наттерер как раз занимался доработкой кода GIMP и GTK+ специально под этот проект.

В итоге было принято решение, что студентка сначала постарается решить более простую задачу, и уже затем, при наличии времени, перейдёт к более сложной задаче. В качестве более простой задачи выбрано упрощение работы с плавающими выделениями.

Как вы знаете, в GIMP с плавающим выделением можно сделать два действия (не считая удаления): вставить его в уже существующий слой или создать новый слой и автоматически поместить в него плавающее выделение. Первое действие выполняется щелчком по холсту или кнопке в диалоге слоёв. Для второго нужно сходить в диалог слоёв и щёлкнуть кнопку создания нового слоя или нажать **Ctrl+Shift+N**. Вместо этого планируется сразу после появления плавающего выделения тем или иным способом прямо на холсте нарисовать интерфейс для выбора действия с этим выделением. Пока что студентка больше изучает код GIMP и экспериментирует с отрисовкой элементов интерфейса на холсте.

**Проект номер три** — реализация инструмента произвольных трансформаций (Cage transform), использующий технологию, в оригинале известную под названием [Green Coordinates](http://www.math.tau.ac.il/~lipmanya/GC/gc_techrep.pdf). Оригинальная технология была разработана для выборочной трансформации трёхмерных моделей. Её переложение на 2D позволит во многих случаях заменить как традиционный инструмент трансформаций по сетке, так и новый инструмент Puppet Warp из Photoshop CS5.

Особый интерес этот инструмент представляет ещё и потому, что это первый полноценный инструмент в GIMP, основанный на GEGL. Разработчики решили, что писать новые инструменты на старом ядре уже попросту неприлично, а Мишель Мюре чётко последовал указаниям.

В настоящее время можно рисовать и редактировать многоугольник, по которому выполняется трансформация. Лежащий в основе инструмента оператор GEGL тоже, в целом, готов. Сейчас Мишель озадачен объединением этого оператора с графическим интерфейсом.

![cage-transform-tool-in-toolbar.png](http://gimp.ru/images/news/gsoc2010-midterm/cage-transform-tool-in-toolbar.png "Инструмент Cage transform в панели инструментов")

Работа над этим проектом ведётся в ветке [soc-2010-cage](http://git.gnome.org/browse/gimp/log/?h=soc-2010-cage) официального репозитория GIMP. На момент публикации новости для успешной сборки этой ветки необходимо совершенно [тривиально пропатчить](http://pellelatarte.fr/en/2010/07/interactions/) GEGL.

Вероятно, вы захотите получить [ответ на вопрос](http://qq.by/), планируется ли включить в состав версии 2.8 два последних проекта GSoC. Отвечаем коротко: нет. Основным приоритетом для разработчиков является скорейший выпуск новой стабильной версии с тем, чтобы уже можно было спокойно перейти к долгожданной окончательной интеграции GEGL.

Попытка включить результаты проектов GSoC в 2.8 означала бы затягивание выпуска этой версии ещё на несколько месяцев, чего, как вы понимаете, никому не хочется. Так что появление нового инструмента трансформации и отрисовки параметров на холсте стоит ждать в первых же версиях новой нестабильной ветки 2.9, ориентировочно — в начале следующего года. А вот работа Дэнни Робсона по всей очевидности будет доступна в одной из ближайших новых версий GEGL.
