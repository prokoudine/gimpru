---
title: "Liquid Rescale 0.7.1"
date: "2010-07-30"
---

Карло Бальдасси обновил сборки для Windows популярного расширения[«Избирательное масштабирование»](http://liquidrescale.wikidot.com/), которое позволяет произвольно масштабировать изображения, оставляя отдельные их части нетронутыми.

Найденная ошибка (падение при попытке запуска) воспроизводится только в Windows, пользователей Linux и Mac OS X это обновление не затрагивает.

[Версия для Windows](http://liquidrescale.wikidot.com/en:download-page-windows) | [Portable для Windows](http://liquidrescale.wikidot.com/en:download-page-windows)
