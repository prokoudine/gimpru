---
title: "GIMP 2.6.10"
date: "2010-07-09"
---

Выпущено обновление стабильной версии [GIMP](http://www.gimp.org/) с исправлением ошибок.

Перечень исправленных ошибок:

- файлы TGA сохраняются с некорректными данными yOrigin в заголовке ([613328](https://bugzilla.gnome.org/show_bug.cgi?id=613328));
- неправильное сохранение файлов Windows Bitmap (BMP) по умолчанию ([623290](https://bugzilla.gnome.org/show_bug.cgi?id=623290));
- неправильно выполняется разбор в CMYK ([621363](https://bugzilla.gnome.org/show_bug.cgi?id=621363));
- в режиме градаций серого неправильно применяется цвет из градиентов ([595170](https://bugzilla.gnome.org/show_bug.cgi?id=595170));
- ошибка в вызове gimp-hue-saturation из PDB ([613838](https://bugzilla.gnome.org/show_bug.cgi?id=613838));
- программа падает при щелчке по полосе прокрутки из раскрывающихся списков ([622608](https://bugzilla.gnome.org/show_bug.cgi?id=622608));
- окно с только что открытым изображением активно, но помещено ниже предыдущего ([565459](https://bugzilla.gnome.org/show_bug.cgi?id=565459)).

Кроме того, обновлены некоторые локализации.

[Ссылки на скачивание](http://gimp.ru/viewpage.php?page_id=8) обновлены. Сборки для Mac OS X как обычно будут объявлены отдельно.
