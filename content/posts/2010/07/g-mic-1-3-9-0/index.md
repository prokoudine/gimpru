---
title: "G'MIC 1.3.9.0"
date: "2010-07-30"
---

Сайт можно переименовывать в gmic.ru :)

В новую версию метафильтра [G'MIC](http://gmic.sourceforge.net/) Давид Шумперле включил:

- новый фильтр Various/Fourier Transform, выполняющий преобразования Фурье;
- новый фильтр Layers/Split Tones, реализующий что-то вроде фильтра постеризации;
- исправлены различные ошибки.

Всего в G'MIC теперь 180 фильтров.

[Скачать](http://gimp.ru/viewpage.php?page_id=13)
