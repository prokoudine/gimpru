---
title: "Сборка 2.6.10 для Mac OS X 10.6"
date: "2010-07-18"
---

С небольшой задержкой выпущена сборка текущей стабильной версии GIMP для Mac OS X 10.6 Snow Leopard.

[Скачать](http://gimp.ru/download/gimp/)
