---
title: "G'MIC 1.3.8.0"
date: "2010-07-23"
---

Давид Шумперле продолжает выпускать обновления популярного метафильтра [G'MIC](http://gmic.sourceforge.net/).

Добавлен новый фильтр Artistic/Rotoidoscope:

[![Фильтр Rotoidoscope](http://farm5.static.flickr.com/4122/4813490006_e0638f7eb8.jpg "Artistic/Rotoidoscope")](http://farm5.static.flickr.com/4122/4813490006_e0638f7eb8_b.jpg)

Как обычно, исправлены различные ошибки кроме, увы, ошибки с кривыми, которую очень сложно воспроизвести.

Помимо того, наконец-то обновлены сборки для Leopard и Snow Leopard, а сборки для Debian и Ubuntu объединены.

[Скачать](http://gimp.ru/viewpage.php?page_id=13)
