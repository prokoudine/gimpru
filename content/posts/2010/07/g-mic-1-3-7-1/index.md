---
title: "G'MIC 1.3.7.1"
date: "2010-07-12"
---

Чего и следовало ожидать: Давид Шумперле выпустил ещё одно обновление метафильтра [G'MIC](http://gmic.eu/gimp.shtml) и несмотря ни на что продолжает надеяться, что это последнее обновление перед отпуском. Мы ему, конечно, снова поверим :)

Исправлены небольшие ошибки, добавлена возможность использовать в сценариях переменные окружения.

Как обычно, все ссылки для закачки есть на [личной странице](http://gimp.ru/viewpage.php?page_id=13) дополнения. Сборки для Mac пока что остались на уровне месячной давности.
