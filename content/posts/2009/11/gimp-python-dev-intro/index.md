---
title: "Введение в написание расширений для GIMP на Python"
date: "2009-11-18"
---

Почти все пользователи Linux для редактирования фотографий используют GIMP. Рано или поздно возникает вопрос о том, как автоматизировать выполнение тех или иных действий.

Конечно, можно написать сценарий на Script-Fu (вариант Scheme для GIMP), но лучше воспользоваться Python, поскольку писать на нём гораздо проще. В этой статье рассматриваются основы написания сценариев на Python.

Что следует знать перед началом работы.

Во-первых, должен быть установлен Python, а также расширение gimp-python, которое позволяет использовать Python для написания расширений.

Во-вторых, расширения, сохранённые в каталог /home/пользователь/.gimp-2.6/plug-ins, будут доступны только одному пользователю, сохранённые в /usr/lib/gimp/2.0/plug-ins — всем.

В-третьих, самостоятельно добавленные расширения должны быть исполняемыми. Есть несколько способов сделать это — от изменения атрибутов через диалог свойств файла в Nautilus или Konqueror до команды _'chmod +x имя\_сценарий.py'_.

Теперь приступим.

1. Запустите GIMP.
2. В меню _«Фильтры > Python-Fu_» выберите пункт _Консоль_.
3. Для загрузки файла и открытия окна GIMP нажмите в открывшемся окне консоли кнопку**Просмотреть**.
4. В появившемся диалоге вы можете просмотреть все процедуры GIMP, достпные для использования в сценариях на Python. Найдите процедуру загрузки файлов JPEG, которая называется file-jpeg-load. Справа в браузере описан тип данных, которые передаются этой процедуре и которые выдаёт она. Чуть ниже приведена краткая справка.
5. Нажмите кнопку «Применить».
6. Измените полученное на _"image = pdb.file\_jpeg\_load(/home/пользователь/image1.jpg, 1)"_. 'pdb' указывает, что используется процедура из базы данных процедур GIMP (Procedure DataBase). Все эти процедуры можно добавлять прямо из браузера, а затем по обстоятельствам редактировать.
7. Теперь переменная image хранит изображение.
8. Посмотреть изображение можно так: _"gimp.Display(image)"_.
9. Для определения рабочей области воспользуйтесь _"drawable = pdb.gimp\_image\_get\_active\_drawable(image)"_.
10. Так можно определить высоту и ширину рабочей области: _"height,width=drawable.height, drawable.width"._
11. Так эти параметры можно посмотреть: _"height, width"._
12. Так можно инвертировать цвета: _"pdb.gimp\_invert(drawable)"._
13. Добавьте новый слой прозрачностью в 50%: _"layer = pdb.gimp\_layer\_new(image, width, height, 0, "new\_layer", 50, 0)"_.
14. Сделайте его видимым в диалоге слоёв: _"image.add\_layer(layer, 0)"._
15. Залейте его цветом, установленным как цвет переднего плана: "_pdb.gimp\_edit\_fill(layer, 1)"._
16. Объедините все видимые слои: _"layer = pdb.gimp\_image\_merge\_visible\_layers(image, 0)"._

По аналогии можно произвести ещё с десяток действий над рисунком, и тогда необходимо будет все их сохранить в файлик с расширением **.py**, который можно повторно использовать.

Расширению можно передавать параметры. Рассмотрим это на примере сценария clothify.py из поставки gimp-python. Пояснения добавлены по ходу текста программы.

* * *

#!/usr/bin/env python

import math #импортируется модуль math
from gimpfu import \* #импортируются функции из GIMP

def python\_clothify(timg, tdrawable, bx=9, by=9,    
                    azimuth=135, elevation=45, depth=3):
#процедуре передаются значения, полученные из диалога; timg и tdrawable 
#говорят о том, что будут использоваться открытый рисунок и рабочая область
    width = tdrawable.width #замеряем высоту
    height = tdrawable.height 

    img = gimp.Image(width, height, RGB)#создаётся рисунок
    img.disable\_undo()
#на время работы программы выключается возможность редактирования 
#рисунка пользователем

    layer\_one = gimp.Layer(img, "X Dots", width, height, RGB\_IMAGE,
                           100, NORMAL\_MODE)#создаётся слой
    img.add\_layer(layer\_one, 0)#слой добавляется в рисунок...
    pdb.gimp\_edit\_fill(layer\_one, BACKGROUND\_FILL)#... и заливается цветом

    pdb.plug\_in\_noisify(img, layer\_one, 0, 0.7, 0.7, 0.7, 0.7)
#используется расширение c соответствующим названием, ему передаются параметры.

    layer\_two = layer\_one.copy()#еще один слой создаётся копированием
    layer\_two.mode = MULTIPLY\_MODE
    layer\_two.name = "Y Dots"#имя слоя
    img.add\_layer(layer\_two, 0)#добавляется слой в рисунок

    pdb.plug\_in\_gauss\_rle(img, layer\_one, bx, 1, 0)
    pdb.plug\_in\_gauss\_rle(img, layer\_two, by, 0, 1)

    img.flatten() #сводятся все слои

    bump\_layer = img.active\_layer #создаётся переменная с текущим активным слоем

    pdb.plug\_in\_c\_astretch(img, bump\_layer)#слой растягивается
    pdb.plug\_in\_noisify(img, bump\_layer, 0, 0.2, 0.2, 0.2, 0.2)
    pdb.plug\_in\_bump\_map(img, tdrawable, bump\_layer, azimuth,
                         elevation, depth, 0, 0, 0, 0, True, False, 0)

    gimp.delete(img)

register(
        "python\_fu\_clothify",#задается название расширения
        "Make the specified layer look like it is printed on cloth",
        "Make the specified layer look like it is printed on cloth",
        "James Henstridge",
        "James Henstridge",
        "1997-1999",
        "<img border="0" />/Filters/Artistic/\_Clothify...",#расширение помещается в меню
        "RGB\*, GRAY\*",
        \[  #создаётся меню для запроса параметров
                (PF\_INT, "x\_blur", "X blur", 9),#запрашиваются параметры 
                (PF\_INT, "y\_blur", "Y blur", 9),
                (PF\_INT, "azimuth", "Azimuth", 135),
                (PF\_INT, "elevation", "Elevation", 45),
                (PF\_INT, "depth", "Depth", 3)
#сначала указывается тип запрашиваемого параметра, затем — переменная, 
#надпись в меню, значение по умолчанию.
        \],
        \[\],
        python\_clothify) 

main()

* * *

На этом всё. Подробнее о создании сценариев на Python для GIMP можно узнать из[документации на английском языке.](http://www.gimp.org/docs/python/index.html)

**Автор**: _Michael_
