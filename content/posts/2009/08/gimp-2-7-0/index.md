---
title: "GIMP 2.7.0"
date: "2009-08-17"
---

Выпущена новая нестабильная версия GIMP. Обзор основных изменений на русском языке [прилагается](http://www.linuxgraphics.ru/articles.php?article_id=72). Полный список изменений — [тут](http://developer.gimp.org/NEWS).

[Скачать](ftp://ftp.gimp.org/pub/gimp/v2.7/gimp-2.7.0.tar.bz2)
