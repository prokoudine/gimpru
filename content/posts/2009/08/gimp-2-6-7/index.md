---
title: "GIMP 2.6.7"
date: "2009-08-14"
---

Выпущена новая стабильная версия GIMP с исправлениями [ошибок](http://developer.gimp.org/NEWS-2.6).

[Скачать](http://gimp.ru/download/gimp/)
