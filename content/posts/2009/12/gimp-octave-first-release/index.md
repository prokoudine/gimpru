---
title: "Выпущена первая версия плагина GIMP Octave"
date: "2009-12-08"
---

El Samuko опубликовал предварительную версию [расширения](http://sites.google.com/site/elsamuko/gimp/gimp-octave), позволяющего редактировать изображения на языке вычислений [Octave](http://www.gnu.org/software/octave/).
