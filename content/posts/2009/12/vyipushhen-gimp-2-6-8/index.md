---
title: "Выпущен GIMP 2.6.8"
date: "2009-12-13"
---

Выпущена новая стабильная версия GIMP. Исправлены ошибки и уязвимости, оптимизирована русская локализация.

[Скачать](ftp://ftp.gimp.org/pub/gimp/v2.6/gimp-2.6.8.tar.bz2) | [Скачать патчем к 2.6.7](ftp://ftp.gimp.org/pub/gimp/v2.6/patch-2.6.8.bz2)
