---
title: "Инсталляторы GIMP для Windows перемещены на основной сервер"
date: "2013-11-05"
description: "В последние месяцы количество жалоб на SourceForge возросло настолько, что мы приняли решение переместить инсталляторы GIMP для Windows на наш основной сервер."
summary: "В последние месяцы количество жалоб на SourceForge возросло настолько, что мы приняли решение переместить инсталляторы GIMP для Windows на наш основной сервер."
---

В последние месяцы количество жалоб на SourceForge возросло настолько, что мы приняли решение переместить инсталляторы GIMP для Windows на наш основной сервер.

SourceForge, бывший полезным и надёжным сервисом разработки и хранения сборок свободного ПО, стал настолько агрессивно использовать рекламу на страницах скачивания, что пользователи перестали ориентироваться в том, где находится кнопка скачивания, а где — реклама, ведущая на сомнительные сайты.

Последней каплей стало решение владельцев сервиса [поставлять инсталляторы СПО с встроенной офферной рекламой](http://www.gluster.org/how-far-the-once-mighty-sourceforge-has-fallen/). Мы не считаем возможным далее поддерживать SourceForge, поэтому решили отказаться от услуг этого сервиса.

Теперь Ежи Симончич, занимающийся сборкой инсталляторов для Windows, размещает файлы прямо на FTP-сервере нашего проекта, откуда они растекаются по зеркалам.
