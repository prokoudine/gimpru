---
title: "Выпущен GIMP 2.8.4"
date: "2013-02-07"
description: "Мы выпустили обновление стабильной версии GIMP. В новой версии — разные исправления, улучшения и обновление переводов интерфейса"
summary: "Мы выпустили обновление стабильной версии GIMP. В новой версии — разные исправления, улучшения и обновление переводов интерфейса"
---

Мы выпустили обновление стабильной версии GIMP. В новой версии — разные исправления, улучшения и обновление переводов интерфейса.

Вот самые важные изменения:

- названия фильтров в диалогах сохранения и экспорта стали более внятными;
- контур кисти теперь перерисовывается намного быстрее;
- состояние окна программы (распахнутое на весь экран) стало запоминаться между сеансами
- инструмент ввода текста теперь работает даже с изображением без слоёв и умеет сохранять форматирование, заданное из диалога параметров инструмента;
- по умолчанию обратному концу стилуса автоматически назначается инструмент_Ластик_.

Русская локализация обновлена и подкорректирована. Во-первых, исправлены некоторые старые ошибки:

![](https://s3.amazonaws.com/gimpru/releases/gimp-2-8-4/gimpmessage.png)

Во-вторых, подчищены конфликтующие ускорители в меню. Например, Alt+З+Alt+И теперь вызывает только диалог смены размера изображения. До этого через раз происходил переход к пункту «Свести изображение».

Ссылки на закачку исходников и сборки для Windows [обновлены](http://gimp.ru/viewpage.php?page_id=8). Сборка для Mac будет готова чуть позднее.
