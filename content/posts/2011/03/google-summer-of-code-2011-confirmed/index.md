---
title: "GIMP участвует в Google Summer of Code 2011"
date: "2011-03-18"
---

Мы снова участвуем в программе Google Summer of Code, уже в четвёртый раз.

В предыдущие годы это участие дало проекту новые инструменты (клонирование по перспективе, лечебная кисть, трансформация по рамке), набор текста прямо на холсте, категоризацию ресурсов с помощью меток, улучшенный интерфейс к динамике рисования, основу для векторных слоёв (интерфейс будет реализован позднее) и т.д.

В этом году приоритетными считаются следующие идеи проектов:

- бесшовное клонирование с адаптацией вставленного фрагмента под яркостные и цветностные характеристики фона, на основе [этой](http://www.cs.huji.ac.il/~danix/mvclone/) научной работы;
- замена виджета GimpSizeEntry; нужно переработать как «начинку», так и интерфейс (например, единица измерения должна показываться в поле ввода);
- дальнейшее превращение фильтров GIMP в операции GEGL;
- реализация единого инструмента трансформации по готовой функциональной спецификации;
- динамически расширяющиеся слои, чтобы пользователи больше не меняли их размер вручную;
- поддержка JavaScript как языка для написания дополнений на основе GNOME Java Script;
- новый виджет для выбора и настройки параметров кисти;
- инструмент для нарезки макетов веб-сайтов;
- интерактивная версия фильтра iWarp (аналог Liquify из Photoshop), работающая прямо на холсте.

Первая из перечисленных идей предложена желающим реализовать её, так что вы вполне можете предложить какие-то свои идеи, но определиться с проектом надо быстро, поскольку заявки от студентов принимаются до 8 апреля включительно. Подробнее о перечисленных идеях вы можете прочитать [здесь](http://wiki.gimp.org/index.php/Hacking:GSoC_2011/Ideas).

Теперь пару слов о самой программе. Для участия в ней нужно быть студентом высшего или специального учебного заведения. Россияне и жители стран СНГ уже не раз участвовали в программе, так что об _этом_ волноваться не стоит. Деньги ($5000) переводятся на банковский счёт.

Что от вас требуется:

- трезво оценить свои силы;
- определиться с проектом;
- связаться с разработчиками (см. ниже), представиться, рассказать о предлагаемом проекте;
- если идея проходит критерии, узнать, кто будет потенциальным руководителем;
- написать заявку с описанием проекта, согласовать её с руководителем;
- до 8 апреля подать заявку;
- убедиться в том, что GIMP нормально собирается из исходного кода, разобраться с основами Git, если ещё не разобрались, поизучать архитектуру;
- быть готовыми в конце мая начать работу.

Если у вас возникают общие вопросы по написанию кода на C, помощь можно получить на каком-нибудь [форуме программистов](http://www.cyberforum.ru/). На частные вопросы по программированию GIMP вам с удовольствием ответят сами разработчики программы.

В июле будет производиться промежуточная оценка результатов. Пока она идёт (обычно около недели), можно взять отпуск и сгонять куда-нибудь отдохнуть. Некоторые умудряются слинять и на две недели, но это уже чревато :)

Что касается своих идей, мы недавно уже писали про то, как именно новый важный функционал будет раскидан по тем или иным версиям. Этот план следует иметь в виду и по возможности выбирать задачи, которые не будут тормозить его выполнение.

Например, единый инструмент трансформации сейчас хоть и запланирован на версию 3.8, но не настолько серьёзно затрагивает ключевые участки кода, чтобы быть помехой: инструменты трансформации в любом случае рано или поздно надо будет переписать на GEGL.

В то же время неразрушающее редактирование нет никакого смысла делать до полного перехода на GEGL и работы с высокой разрядностью на цветовой канал. Словом, советуйтесь с разработчиками.

В остальном, рекомендуем почитать [статью о GSoC](http://linuxgraphics.ru/articles.php?article_id=66) на линуксграфиксе. Ей почти два года, но суть от того не поменялась. Ну и конечно можно задавать уточняющие вопросы в комментариях.
