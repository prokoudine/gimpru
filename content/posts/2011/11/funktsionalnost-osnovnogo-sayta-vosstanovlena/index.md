---
title: "Функциональность основного сайта восстановлена"
date: "2011-11-01"
---

В начале октября Университет Беркли отказался от дальнейшего размещения данных проекта, поэтому некоторое время часть ресурсов GIMP была недоступна. К настоящему моменту действие всех сервисов возобновлено.

В полном объёме функционируют: FTP-сервер, онлайновая версия документации, раздел для разработчиков, вики. Списки рассылки перенесены на gnome.org, читатели автоматически переподписаны. Новые названия списков рассылки:

- [gimp-developer-list](http://mail.gnome.org/mailman/listinfo/gimp-developer-list)
- [gimp-user-list](http://mail.gnome.org/mailman/listinfo/gimp-user-list)
- [gimp-docs-list](http://mail.gnome.org/mailman/listinfo/gimp-docs-list)
- [gimp-web-list](http://mail.gnome.org/mailman/listinfo/gimp-web-list)
- [gegl-developer-list](http://mail.gnome.org/mailman/listinfo/gegl-developer-list)

Списки рассылки gimp-announce, gimp-film, and gimp-win-user больше не функционируют. Архивы списков за всё прошедшее время будут доступны позднее, а пока что можно пользоваться gmane и прочими подобными сервисами.
