---
title: "UFRaw 0.18"
slug: ufraw-0-18
date: "2011-02-21"
summary: "Уди Фукс (Udi Fuchs) выпустил новую версию UFRaw — программы для работы со снимками RAW, которая может работать расширением GIMP."
description: "Уди Фукс (Udi Fuchs) выпустил новую версию UFRaw — программы для работы со снимками RAW, которая может работать расширением GIMP."
---

Уди Фукс (Udi Fuchs) выпустил новую версию [UFRaw](http://gimp.ru/viewpage.php?page_id=11) — программы для работы со снимками RAW, которая может работать расширением GIMP.

В этой версии нет никаких функциональных новшеств. Изменения включают  добавленную локализацию на упрощённый китайский язык и поддержку следующих 39 моделей камер:

- Canon PowerShot G12, S95, SX120 IS (CHDK hack), SX20 IS (CHDK hack);
- Casio EX-Z1050, Casio EX-Z1080;
- Fuji FinePix HS10/HS11;
- Hasselblad H4D;
- Kodak Z981;
- Nikon D3s, D3100, D7000, Coolpix P7000;
- Nokia X2;
- Olympus E-5, E-P2, E-PL1, E-PL2;
- Panasonic DMC-FZ40, DMC-FZ100, DMC-G2, DMC-GF1, DMC-GF2, DMC-LX5;
- Pentax K-r, K-5, 645D;
- Samsung EX1, GX20, NX100, WB550, WB2000;
- Sony DSLR-A450, DSLR-A580, NEX-3, NEX-5, SLT-A33, SLT-A55V.

Вы можете скачать:

- [исходный код](http://sourceforge.net/projects/ufraw/files/ufraw/ufraw-0.19.2/ufraw-0.19.2.tar.gz/download);
- [сборку для Windows](http://sourceforge.net/projects/ufraw/files/ufraw/ufraw-0.19.2/ufraw-0.19.2-2-setup.exe/download).
