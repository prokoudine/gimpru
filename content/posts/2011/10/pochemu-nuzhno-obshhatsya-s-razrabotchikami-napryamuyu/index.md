---
title: "Почему нужно общаться с разработчиками напрямую"
date: "2011-10-18"
---

Тезис, почему надо не стесняться и общаться с разработчиками напрямую, редакция GIMP.ru хочет проиллюстрировать свежим примером.

- 17/10/2011 10:51, sergeG: «Очень бы хотелось убедить разработчиков включить в 2.8 патч из [https://bugzilla....?id=630028](https://bugzilla.gnome.org/show_bug.cgi?id=630028 "https://bugzilla.gnome.org/show_bug.cgi?id=630028")»
- 17/10/2011 14:10, <muks> i'll try that this week and if all goes well, will commit it
- 17/10/2011 18:34, <muks> the patch works ok for me, \* muks is wondering if the fact that it works properly is good enough to commit it, without checking the modifications
- 17/10/2011 23:50, <CIA-8> mitch \* [f92df81a47da](http://git.gnome.org/browse/gimp/commit/?id=f92df81a47da588986c69e36bdf82aa6335559be) gimp/app/paint/gimpheal.c: Bug 630028 - Improvement of the healing tool

Надеемся, всё понятно :) Было:

![Цветные артефакты](http://i.minus.com/i1tGqEG3kYTj.jpg "Цветные артефакты")

Стало:

![Никаких артефактов](http://i.minus.com/iYzbCl1pZLB0A.jpg "Никаких артефактов")

А всего-то надо было зайти на IRC и показать на готовый патч ещё раз :)

Ждите в версии 2.7.4.
