---
title: "Как создать гранжевую кисть в GIMP"
date: "2010-04-22"
author: Watup
summary: "Пройдя этот урок, вы научитесь делать шершавые гранжевые кисти."
description: "Пройдя этот урок, вы научитесь делать шершавые гранжевые кисти."
tags: ['ресурсы']
---

Несмотря на большую популярность готовых кистей, градиентов и текстур, которые зачастую можно [скачать бесплатно](http://kavchenkova.ru/free-patterns.htm), многие серьёзные художники создают такие ресурсы себе сами.

Пройдя этот урок, вы научитесь делать шершавые гранжевые кисти.

1\. Создайте новое изображение размером 350×350 пикселов с белым фоном.

2\. Добавьте четыре прозрачных слоя.

3\. Поменяйте их режимы смешивания следующим образом:

![layers.png](layers.png "Слои")

4\. Перейдите на прозрачный слой с обычным режимом смешивания и вызовите диалог фильтра «Плазма» (_«Фильтры > Визуализация > Облака > Плазма»_). Укажите следующие параметры и примнените фильтр, не забыв нажать кнопку «Новое зерно» для получения случайного значения:

![plasma.png](plasma.png)

5\. Обесцветьте этот слой:

![desaturation.png](desaturation.png)

6\. Повторите шаги 4 и 5 в слое с режимом «Объединение зерна».

Результат:

![step6.jpg](step6.jpg)

7\. Вызовите диалог фильтра «Гимпрессионист» (_«Фильтры > Имитация > Гимпрессионист»_), выберите в нём текстуру _Painted Rock_ и примените фильтр.

[![gimpressionist-th.png](gimpressionist.png)](gimpressionist.png)

Результат:

![step7.jpg](step7.jpg)

8\. В слое с режимом смешивания «Умножение» вызовите диалог фильтра «Сплошной шум» (_«Фильтры > Визуализация > Облака > Сплошной шум»_), укажите приведённые ниже параметры, несколько раз нажав кнопку «Новое зерно» для получения случайного значения, и примените фильтр.

![solid-noise-1.png](solid-noise-1.png)

Результат:

![step8.jpg](step8.jpg)

9\. Повторите шаг 8 для слоя с режимом смешивания «Деление», но с такими параметрами:

![solid-noise-2.png](solid-noise-2.png)

Результат:

![step9.jpg](step9.jpg)

10\. Объедините все слои в один (_«Изображение > Свести изображение»_).

11\. Теперь нужно задать конечную форму кисти. Это можно сделать разными способами. Если геометрически точная окружность или овал вас устраивают, переключитесь на инструмент создания эллиптических выделений (**E**) и нарисуйте окружность от центра. Для этого включите соответствующую галку в параметрах инструмента:

![circular-selection.png](circular-selection.png)

Другой способ, предлагаемый автором урока, состоит в рисовании фигуры инструментом _Контуры_с последующим созданием выделения из полученного контура (щёлкните кнопку «Выделение из контура» в диалоге параметров инструмента).

12\. Границы выделения стоит сделать менее резкими. Для этого растушуйте его на 40-80 пикселов, как вам больше нравится (_«Выделение > Растушевать»_).

13\. Теперь необходимо удалить всё, что за пределами выделения. Инвертируйте выделение (**Ctrl+I**, либо _«Выделение > Инвертировать»_) и удалите содержимое нового выделения (**Del**, либо _«Правка > Очистить»_).

![erasing-selection-outside.png](erasing-selection-outside.png)

14\. Уберите ненужное пустое пространство командой _«Изображение > Автокадрировать изображение»_.

15\. Переведите изображение в режим оттенков серого цвета командой _«Изображение > Режим > Градации серого»_.

16\. Сохраните полученное изображение как кисть (начиная с версии 2.7.0 это выполняется через_«Файл > Экспортировать»_).

![export-as-brush.png](export-as-brush.png)

В диалоге со списком кистей вы увидите созданную вами кисть:

![brushes-list.png](brushes-list.png)

Вот и всё.

Оригинал: http://www.gimptalk.com/forum/making-grunge-brushes-t5443.html
