---
title: "Восстановление детализации ярких участков фотоснимков"
date: "2009-11-18"
summary: "Если вы снимаете в RAW, восстановить детали ярких участков изображения очень просто."
description: "Если вы снимаете в RAW, восстановить детали ярких участков изображения очень просто."
---

Если вы снимаете в RAW, восстановить казалось бы утерянные из-за переэкспонирования детали ярких участков изображения очень просто. Вам всего лишь нужны GIMP и UFraw.

_Примечание: в настоящее время проблема «розовых облаков» в современном ПО для работы с RAW решена практически полностью и возникает, по большому счёту, лишь в отдельных случаях, когда производители камер делают ошибки. Июль 2010._

Откройте снимок в GIMP. Для его предобработки автоматически загрузится UFRaw. Для начала убедитесь в том, что коррекция клиппинга ярких участков [фото](http://bumpme.ru/) (Unclip highlights) выключена, затем включите индикатор переэкспозиции (overexposure). Теперь перетащите регулятор экспозиции до положения, при котором все переэкспонированные области (чёрные на изображении) не исчезнут. Диалог UFRaw при этом будет выглядеть примерно так:

![](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-ufraw_unclipped_color_cast.png)

Нажмите ОК для запуска обработки снимка и его открытия в GIMP.

Теперь откройте исходный снимок в RAW ещё раз. Все параметры должны быть теми же, что и в предыдущий раз, но теперь коррекция клиппинга ярких участков должен быть включена. Диалог должен выглядеть так:

![ ](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-ufraw_clipped_color_cast.png)

Ещё раз запустите обработку снимка. Теперь у вас два открытых варианта снимка. В первом создайте новый пустой слой и вставьте туда второй вариант снимка через буфер обмена, изменив затем тип наложения для нового слоя с _Нормального_ на _Насыщенность_ (_Saturation_). Диалог слоёв будет выглядеть так:

![](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-gimp_layers.png)

Яркие участки теперь должны быть обесцвечены при помощи маскирующего первого слоя, а искажение цвета должно исчезнуть. Подберите значение непрозрачности верхнего слоя. Когда устраивающий вас вариант будет получен, сведите слои и сохраните файл.

Вот пример:

[![](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-invalides_eiffel_unclipped_wrong_colors_mini.jpg)](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-invalides_eiffel_unclipped_wrong_colors.jpg)

Первое изображение: все детали ярких участков присутствуют, но цвета потеряны (например, небо приобрело розовый оттенок).

[![](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-invalides_eiffel_clipped_mini.jpg)](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-invalides_eiffel_clipped.jpg)

Второе изображение: искажение цвета пропало, но переходная область имеет зеленоватый оттенок и многие детали утеряны, а облака еле различимы.

[![](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-invalides_eiffel_final_mini.jpg)](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-invalides_eiffel_final.jpg)

Результат: яркие участки изображения сохраняют детали без цветовых искажений.

Вот второй пример:

[![](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-kazimierz_unclipped_mini.jpg)](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-kazimierz_unclipped.jpg)

На снимке выше детали хорошо различимы, но яркий участок закрашен в розовый цвет.

[![](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-kazimierz_clipped_mini.jpg)](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-kazimierz_clipped.jpg)

На втором изображении детали из яркой области снимка отсутствуют — к примеру, в ветвях — а переходная область имеет странный жёлтый оттенок — один или два из трёх каналов «потеряны».

[![](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-kazimierz_final_mini.jpg)](http://www.linuxgraphics.ru/images/articles/dphoto/highlights-restoration/tutorial-dphoto-highlights-restoration-kazimierz_final.jpg)

В итоге мы получили изображение, на котором видны детали яркой части снимка без странного жёлтого оттенка.

Известна ещё одна методика: "[Using B&W To Improve Your Color Images And Increase Dynamic Range](http://web.archive.org/web/20160320053126/http://visual-vacations.com/Photography/hybrid_raw_conversion.htm)".

* * *

**Автор**: Cyril Guyot **Оригинал**: [How to recover seemingly lost highlight detail](http://people.zoy.org/~cyril/ufraw_highlight_recovery/)  **Перевод**: Александр Прокудин **Лицензия**: не указана автором, публикация с его любезного разрешения
