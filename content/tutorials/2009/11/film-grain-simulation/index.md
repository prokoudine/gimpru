---
title: "Имитация пленочного зерна в GIMP"
date: "2009-11-18"
---

Одним из самых привлекательных для потребителей преимуществ цифровых камер над пленочными является отсутствие пленочного зерна на шелковисто-гладких изображениях, полученных с их помощью (подробнее о причинах появления зернистости на плёнке можно почитать по ссылкам в конце статьи). С цифровой камерой при высокой светочувствительности (измеряемой в единицах ISO) вы, конечно, можете получить до известной степени аналог пленочного зерна — шум сенсора.

И все же, несмотря на то, что чаще всего я хочу снизить шумы на цифровых изображениях до минимума, иногда хочется, напротив, добавить на изображение эту зернистость, особенно на черно-белые фото.

В этом руководстве я покажу вам, как сымитировать пленочное зерно в цифре при помощи GIMP. Я думаю, портрет этого добродушного уличного музыканта будет хорошо выглядеть с пленочным зерном. Чтобы лучше понять, о чем идет речь, внимательно рассмотрите предложенные фотографии, уделяя особое внимание сравнению стены, пиджака, шляпы и самого лица.

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/1.jpg)

До имитации пленочного зерна:

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/3.jpg)

После имитации пленочного зерна:

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/4.jpg)

### Имитация пленочного зерна

Вся прелесть этого метода состоит в использовании слоев. Зернистость добавлена ко второму слою в режиме _«Перекрытие»_; исходное изображение при этом остается нетронутым в своем слое.

Откройте изображение в GIMP. Если вы хотите сделать из цветного фото черно-белое, сначала следует перевести его в ч/б. Обратите внимание, что эта техника работает только с изображениями в режиме RGB.

Убедитесь, что работаете с копией исходного слоя (нажмите **Ctrl+D** для копирования), так как немного позднее нам понадобится исходное изображение. Двойным щелчком по индикатору цвета переднего плана (черный квадрат на иллюстрации ниже) в панели инструментов GIMP вызовите диалог выбора цвета.

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/5.png)

Установите следующие значения:

Красный = 128 Зеленый = 128 Синий = 128

и нажмите «ОК». Таким образом, в качестве цвета переднего плана вы выберете нейтрально-серый (50%).

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/6.png) ![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/7.png)

Вызовите диалог слоев (Ctrl+L) и создайте новый слой:

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/8.png)

В открывшемся диалоговом окне введите имя слоя «Плёночное зерно» и выберите _«Цвет переднего плана»_ цветом заливки.

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/9.png)

Вы увидите, что ваше изображение скрылось за новым серым слоем. В раскрывающемся списке диалога слоев _«Режим»_ выберите режим наложения _«Перекрытие»_. Теперь вы снова видите свое изображение.

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/10.png)

Выберите слой «Плёночное зерно» в диалоге слоев и вызовите фильтр «Шум HSV» (_«Фильтры -> Шум -> Шум HSV»_). Этот фильтр добавит шум к серому слою, который будет накладываться на изображение. Ниже предложено описание параметров фильтра с точки зрения использования фильтра для имитации зернистости.

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/11.png)

- _«Фиксированность»_: этот параметр будет менять интенсивность и степень зернистости. Начните менять его после того, как изменили параметр _«Яркость (Value)»_. Большие значения делают зернистость менее заметной.
- _«Тон»_: если значение параметра _«Насыщенность»_ равно нулю, единственный эффект от изменения этого параметра — изменение шаблона зернистости. Обычно я устанавливаю значение этого параметра равным нулю.
- _«Насыщенность»_: установите значение этого параметра равным нулю, если, конечно, вы не хотите получить цветное зерно (например, для цветного фото). В случае с цветным зерном необходимо также изменить значение этого параметра вместе со значением тона.
- _«Яркость»_: этот параметр будет решающим при изменении интенсивности и степени зернистости. Увеличение этого параметра приведет к увеличению контраста зерен и их размера, что приведет к укрупнению и затемнению зерен. Я советую вам увеличивать этот параметр постепенно, начиная с 1 и до тех пор, пока вы не получите желаемую интенсивность в области предпросмотра, а затем увеличивать параметр _«Фиксированность»_также от 1, чтобы посмотреть, какой эффект он дает.

Экспериментируйте с параметрами, до тех пор, пока не получите устраивающий вас шаблон зернистости в области предпросмотра фильтра, затем нажмите "ОК". После того как фильтр закончит свою работу, вы увидите, что ваше изображение приобрело зернистую текстуру.

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/12.jpg)

Если вас не устроил результат, отмените последнее изменение (**Ctrl+Z**) и снова примените тот же самый фильтр, но с другими значениями параметров (**Shift+Alt+F**). Не переживаете, если зерно получилось слишком заметным, потому что на следующем шаге мы сделаем его мягче.

Итак, если зерно показалось вам слишком заметным, примените фильтр _«Гауссово размывание»_(_«Фильтры -> Размывание -> Гауссово размывание»_) с небольшим радиусом и типом размывания IIR. Обратите внимание, что радиус размывания должен быть именно небольшим, иначе вы потеряете желаемую зернистость изображения, и снимок будет содержать шум, но не зернистость.

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/13.png)

Значение в интервале 1-3 должно быть подходящим, только если вы использовали большое значение _«Яркость»_ в фильтре _«Шум HSV»_. Если вам понравилось полученное зерно, пропустите этот шаг.

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/14.jpg)

Если же вас не устроил результат работы фильтра, отмените последнее действие (**Ctrl+Z**) и примените эффект заново с новыми параметрами (**Shift+Alt+F**).

На этом шаге можно остановиться, но я обычно делаю немного больше.

Обычно пленочное зерно более заметно на средних тонах и значительно менее заметно на теневых и освещенных участках. С помощью маски слоя можно изменить способ наложения зернистой текстуры на изображение, чтобы достичь этого.

Щелкните правой кнопкой мыши слой «Плёночное зерно» и выберите в открывшемся меню пункт_«Добавить маску слоя»_. В открывшемся диалоговом окне выберите _«Белый цвет (полная непрозрачность)»_. Вообще-то, все равно что вы выберите здесь: все равно мы вставим другое изображение в маску.

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/15.png) ![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/16.png)

В диалоге слоев щелкните слой «Копия фона», переключитесь обратно в окно с изображением, выделите все и скопируйте в буфер обмена (**Ctrl+A**, затем **Ctrl+C**). В диалоге слоев щелкните значок маски слоя «Плёночное зерно» (маленький белый прямоугольник). Затем снова переключитесь в окно с изображением и вставбте изображение из буфера обмена (**Ctrl+V**). Затем в диалоге слоев нажмите кнопку _«Прикрепить плавающий слой»_, чтобы прикрепить вставленное изображение к маске слоя.

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/17.png)

Не убирая выделение с маски слоя (оно должно было сохраниться на ней после вставки), откройте диалог «Кривые» (_«Цвет -> Кривые»_). Щелкните по середине прямой, чтобы добавить точку. Затем перетащите точку из правого верхнего угла вниз, в правый нижний угол, как это показано на иллюстрации ниже.

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/18.png) ![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/19.png)

Вы только что «наполовину инвертировали» маску слоя: заменили все освещенные места тенями, так что средние тона стали самими яркими на снимке. Если зерно выглядит слишком приглушенно, вам придется перетащить среднюю точку выше, увеличив таким образом яркость средних тонов. Следите за тем, как изменяется яркость в окне изображения. Для завершения нажмите ОК.

![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/20.jpg) ![](http://www.linuxgraphics.ru/images/articles/dphoto/gimp-film-grain/21.jpg)

Чтобы посмотреть, какой эффект дает маска слоя, нажмите на значок маски слоя в диалоге слоев, удерживая нажатой клавишу **Ctrl**: вокруг значка появится красный прямоугольник, и в окне изображения вы увидите вашу работу без эффекта, который дает маска. Включив маску, вы должны заметить едва уловимое отличие от изображения без маски. Возможно, вам захочется размыть зерно. Не забудьте предварительно щелкнуть значок слоя с зерном в диалоге слоев, иначе вы размоете не слой, а его маску!

Наконец, если вы хотите позднее изменить изображение, стоит сохранить вашу работу под новым именем или скопировать изображение (**Ctrl+D**) и свести его (_«Изображение -> Свести изображение»_).

Вы можете поэкспериментировать с тоном и насыщенностью, изменить контраст с помощью «Уровней» и «Кривых», но уже на сведенном изображении.

### Советы

1. Вы можете изменить прозрачность слоя «Плёночное зерно», используя ползунок в диалоге слоев. Это простой способ уменьшить интенсивность зерна.
2. Вы можете изменить форму кривой для маски слоя, чтобы подобным образом изменить интенсивность зерна, но одновременно изменить и тональность областей, как мы это сделали.
3. Вы можете использовать «Уровни», «Кривые» или любые другие инструменты на слое «Плёночное зерно», чтобы улучшить эффект зернистости. Экспериментируйте!
4. Попробуйте другой тон серого или просто другой цвет на слое «Плёночное зерно».
5. Фильтр «Шум RGB» (_«Фильтры -> Шум -> Шум RGB»_) тоже можно использовать для создания зерна. Он больше подходит в тех случаях, когда вы хотите добавить цветное зерно (вы можете выбрать соотношение красного синего и зеленого). Я думаю, что для черно-белого зерна больше подходит фильтр «Шум HSV».
6. Эта техника может быть скомбинирована с другими, представленными на сайте, например тонированием изображения в стиле «сепия».

Что еще можно почитать о плёночном зерне

1. Kodak: H-1 Exposure Information: [Understanding Graininess and Granularity](http://www.amazon.com/Understanding-Graininess-Granularity-1986-Paper/dp/B007IMVXFY).
2. [Understanding Film Grain and Aliasing](http://www.photoscientia.co.uk/Grain.htm).
3. [Дискуссия о том, что находится в красном, зеленом и синем каналах цифрового изображения](http://web.archive.org/web/20031003182944/http://www.microweb.com/idig/content/seybold/imgchanl.htm).

* * *

**Автор**: Eric R. Jeschke **Источник**: [FilmGrain](https://gimpguru.wordpress.com/Tutorials/FilmGrain/) **Перевод**: madhat87 **Лицензия на текст**: [CС BY NC SA](http://creativecommons.org/licenses/by-nc-sa/2.0/) **Лицензия на фотографии**: [CC BY NC ND](http://creativecommons.org/licenses/by-nc-nd/2.0/)
