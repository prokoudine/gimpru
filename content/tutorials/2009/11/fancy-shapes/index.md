---
title: "Эффект с занятными фигурами в GIMP"
date: "2009-11-18"
author: Starlight86
#featured_image: 'fancy-shapes.jpg'
#omit_header_text: true
tags: ['композиции']
summary: "Совсем недавно Abduzeedo опубликовал урок, демонстрирующий создание изображения в стиле Энди Гилмора (Andy Gilmore). Мне понравилась эта техника, и я решил, что было бы здорово сделать что-то наподобие в GIMP."
description: "Совсем недавно Abduzeedo опубликовал урок, демонстрирующий создание изображения в стиле Энди Гилмора (Andy Gilmore). Мне понравилась эта техника, и я решил, что было бы здорово сделать что-то наподобие в GIMP."
---

Совсем недавно Abduzeedo опубликовал урок, [демонстрирующий](http://abduzeedo.com/crazy-cool-vectors-illustrator-and-photoshop) создание изображения в стиле Энди Гилмора (Andy Gilmore). Мне понравилась эта техника, и я решил, что было бы здорово сделать что-то наподобие в GIMP. Так что вот мой новый урок. Перед началом работы давайте посмотрим на мой результат:

![Результат](2969445856_53ba316964.jpg)

Эту идею я уже недавно иллюстрировал в блоге, когда писал об экспериментах с режимами наложения слоев. Что касается фигуры, я создал собственную кисть в Illustrator и преобразовал ее в формат Photoshop, но не волнуйтесь, потому что GIMP умеет открывать кисти в этом формате, так что у вас не будет с ней проблем. надеюсь, что вам понравится этот урок, и вы оставите мне комментарий по поводу того, что думаете об уроке. Мне это поможет.

Скачайте файл кисти [отсюда](http://starsys.deviantart.com/art/water-drop-vertical-shapes-101417024).

Создайте новый документ. Я использовал 1024×768. Залейте фоновый слой чёрным цветом.

![1](01.png)

Создайте новый слой и назовите его по имени градиента (см. далее). Сделайте цветом переднего плана b10993.

![2](02.png)

Переключитесь на инструмент градиентной заливки, выберите градиент _Основной в прозрачный_радиальной формы и понизьте непрозрачность заливки до 60%. Залейте слой градиентом.

![3](03.png)

Далее по ходу урока нам придётся выравнивать фигуры, для его заранее стоит создать направляющую линию:

![4](04.png)

Создайте новый слой и назовите его «Фигура 1». Переключитесь на Кисть (**P**), выберите установленную кисть и переключитесь на белый цвет переднего плана, затем один нарисуйте ею одну фигуру:

![5](05.png)

Добавьте в этот слой маску, инициализировав её с белым цветом. Переключитесь на инструмент градиентной заливки, убедитесь в том, что активный градиент — _Основный в прозрачный_. Сделайте цвет переднего плана чёрным и сверху вниз нарисуйте инструментом примерно такой отрезок:

![6](06.png)

Получится:

![7](07.png)

Примените получившуюся маску слоя (_«Слой > Маска > Применить маску слоя»_, либо через контекстное меню слоя). Затем продублируйте активный слой с фигурой и переименуйте получившийся слой в «Фигура 2».

![8](08.png)

Повторите процедуру дублирования, пока не получите пять слоёв с фигурами как на иллюстрации ниже. Не забудьте поименовать слои.

![9](09.png)

Создайте ещё один прозрачный слой и назовите его «Фигура 6», но не создавайте маску слоя для него.

![10](10.png)

Продублируйте слой «Фигура 6», переименуйте полученный слой в «Фигура 7» и сместите его ещё правее, вот так:

![11](11.png)

Поменяйте режим наложения слоёв 1,2,3,4,5 на _Объединение зерна_, а слои 6 и 7 оставьте с обычным режимом.

![12](12.png)

Создайте новый слой, залитый плоским белым цветом, и назовите его «Облака». Вызовите фильтр эффекта _«Фильтры > Визуализация > Облака > Разностные облака»_. Используйте значение 4 для детализации, а также включите перемешивание и мозаичность.

![13](13.png)

Примените фильтр, измените режим слоя на _Перекрытие_.

![14](14.png)

Создайте новый прозрачный слой с названием «Спектральный градиент». Переключитесь на инструмент градиентной заливки, выберите из набора градиентов _Full Saturation Spectrum CCW_, укажите 100% непрозрачность и линейную форму. Проведите инструментом заливки отрезок примерно из точки пересечения направляющих до правого верхнего угла. Поменяйте режим слоя на _Перекрытие_.

![15](15.png)

Создайте поверх рисунка ещё один слой и залейте его чёрным цветом. Примените к нему фильтр «Освещение» (_«Фильтры > Свет и тень > Освещение...»_) с параметрами как на снимке экрана ниже.

![16](16.png)

Результат:

![17](17.png)

Поменяйте режим слоя на _Перекрытие_:

![18](18.png)

Теперь давайте добавим текстуру бумаги. Скачайте её [отсюда](http://princess-of-shadows.deviantart.com/art/vintage-grunge-textures-79040200). Файл texture1.jpg вполне подойдёт: просто перетащите его в окно с нашим проектом, и он будет добавлен над активным слоем, т.е. в самый верх стопки слоёв:

![19](19.jpg)

Поменяйте режим слоя на _Перекрытие_ и уменьшите непрозрачность до 40%:

![20](20.png)

Создайте новый слой и залейте его чёрным цветом. Вызовите диалог фильтра «Отблеск линзы» (_«Фильтры > Свет и тень > Отблеск...»_), поменяйте параметры центра отблеска на 307:295.

![21](21.png)

Сместите слой примерно как на иллюстрации ниже и смените его режим на _Экран_.

![22](22.png)

Поскольку слой слишком яркий, следует немного скорректировать уровни. Вызовите диалог коррекции цветовых уровней и поменяйте уровни на входе на 50, 1, 255.

![23](23.png)

Результат:

![24](24.png)

Удалите крупной полупрозрачной растушёванной стёркой большую часть второстепенных отблесков и красный круг вокруг основной светящейся точки.

![25](25.png)

Дважды продублируйте полученный слой с отблеском и разместите копии, скажем, на пересечениях контуров фигур.

![26](26.png)

Наконец добавьте вверх слой, залитый чёрным цветом, и смените его режим на _Перекрытие_, чтобы добавить изображению глубины. Вот и результат!

[![starlight](cool_shapes_th.jpg)](cool_shapes.jpg)

Вы можете [скачать файл XCF](cool_shapes.xcf.bz2).

Оригинал урока: https://puteraaladin.blogspot.com/2008/10/gimp-cool-shapes-effect-tutorial.html
