---
title: "Эффект бокэ в дизайне"
author: Александр Прокудин
date: "2009-11-18"
summary: "Описываемый приём в последнее время можно встретить практически везде: обои рабочего стола, заставки и даже наружная реклама. Так что если вас интересовало, как это делается — урок как раз для вас."
---

Описываемый приём в последнее время можно встретить практически везде: обои рабочего стола, заставки и даже [наружная реклама](http://www.proreklamy.com.ua/index/naruzhnaja_reklama/0-38). Так что если вас интересовало, как это делается — урок как раз для вас.

Перед началом работы скачайте кисть Eclipse на [Deviantart](http://starsys.deviantart.com/art/GIMP-BOKEH-BRUSH-111475344) и положите файл GBR в каталог с кистями GIMP, например, ~/.gimp-2.6/brushes/.

Вот образец, по которому мы будем работать:

[![Образец](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/preview.jpg)](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/full_preview.jpg)

### Процесс

Создайте новый документ. Я использовал 1600×1200 пикселов. Залейте фоновый слой чёрным цветом.

![1](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/01.png)

Создайте новый слой. Я назвал его «Фиолетовый». Выберите тёмно-лиловый цвет. Я использую #b10993.

![2](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/02.png)

Переключитесь на инструмент градиентной заливки, выберите градиент _Основной в прозрачный_, форму_Радиальная_, понизьте непрозрачность до 50% и нарисуйте примерно такой отрезок почти от центра до почти края:

![3](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/03.png)

Результат:

![4](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/04.png)

Теперь создайте новый полупрозрачный слой. Я назвал его «Затмение 1». Переключитесь на инструмент _Кисть_и выберите кисть Eclipse. Используйте белый цвет для кисти. Затем укажите следующие параметры динамики кисти:

![5](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/05.png)

Теперь нарисуйте несколько кругов в слое «Затмение 1».

![6](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/06.png)

Зайдите в _«Фильтры > Размывание > Гауссово размывание»_. Для этого первого слоя используйте размывание на 40 пикселов по горизонтали и вертикали.

![7](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/07.png)

Результат:

![8](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/08.png)

Создайте новый слой (я назвал его «Затмение 2») и снова начните рисовать.

![9](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/09.png)

Зайдите в _«Фильтры > Размывание > Гауссово размывание»_. Для этого первого слоя используйте размывание на 10 пикселов по горизонтали и вертикали.

![10](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/10.png)

Создайте еще один слой и повторите предыдущий шаг, размыв получившийся слой только на один пиксел в обоих направлениях.

![11](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/11.png)

Создайте новый слой. Я назвал его «Облака». Залейте его белым цветом. Зайдите в _«Фильтры > Визуализация > Облака > Разностные облака»_ и примените этот эффект к слою. Используйте значение 4 для детализации, а также включите перемешивание и мозаичность. После этого измените режим слоя на_Перекрытие_.

![12](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/12.png)

Результат:

![13](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/13.png)

Создайте новый полупрозрачный слой. Я назвал его «Градиент». Переключитесь на инструмент градиентной заливки, выберите из набора градиентов _Full Saturation Spectrum CCW_, укажите 100% непрозрачность и линейную форму. Проведите инструментом из правого верхнего угла в левый нижний. Поменяйте режим слоя на _Перекрытие_. Вы увидите вот такой занятный эффект:

![14](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/14.png)

Перейдите к слою «Затмение 3» и поменяйте режим на _Объединение зерна_.

![15](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/15.png)

Наконец, перейдите к слою «Затмение 2» и поменяйте его режим на _Направленный свет_.

![16](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/16.png)

Готово!

[![Результат](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/bokeh_th.jpg)](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/bokeh.jpg)

Та же композиция, но с градиентом _Tropical colors_:

![Tropical colors](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/tropical_colors.jpg)

А вот мой же результат, полученный при помощи Photoshop. Как видите, при помощи GIMP можно получить практически такой же результат.

[![](http://2.bp.blogspot.com/_R8FhZZa6Dro/SPefDARUQFI/AAAAAAAACDM/Be88kkZGwJg/s400/awesome+bokeh.jpg)](http://www.flickr.com/photos/27269808@N08/3244526653/)

Надеюсь, что вам понравилось. Вся признательность — Fabio Sasso, который это придумал.

Файл XCF [прилагается](http://linuxgraphics.ru/images/articles/fx/bokeh-effect-in-gimp/bokeh.xcf.bz2).
