---
title: "GIMP"
url: "/download/gimp/"
summary: "Скачать GIMP для Windows, macOS и Linux"
description: "Скачать GIMP для Windows, macOS и Linux"
type: "page"
featured_image: ''
disable_date: true
---

# Стабильная версия

## Windows

Инсталлятор для Windows включает сборку, которая работает в 32- и 64-разрядных системах. Документация поставляется отдельно.

[Скачать GIMP 2.10.20](https://download.gimp.org/mirror/pub/gimp/v2.10/windows/gimp-2.10.20-setup-1.exe)

Минимальные требования:

- Windows 7
- 256 МБ ОЗУ;
- Pentium III, Athlon XP и выше.

## macOS

Актуальные сборки для Mac не требуют установки X11. Поддерживается самая новая версия macOS. Для установки просто откройте скачанный DMG и перетащите «GIMP.app» в папку «Applications».

[Скачать GIMP 2.10.14](https://download.gimp.org/mirror/pub/gimp/v2.10/osx/gimp-2.10.14-x86_64-1.dmg)

## Linux

Для установки GIMP в Linux мы рекомендуем пользоваться официальными сборками для вашего дистрибутива. Обратитесь к документации по установке приложений в вашем дистрибутиве Linux. Вы также можете скачать сборку этой версии в универсальном формате Flatpak.

[Скачать GIMP 2.10.20 во Flatpak](https://flathub.org/repo/appstream/org.gimp.GIMP.flatpakref)

## Исходный код

Вы можете собрать GIMP из исходного кода самостоятельно. Получить исходники можно двумя способами:

- скачать [архив выпущенной версии](https://download.gimp.org/pub/gimp/v2.10/gimp-2.10.20.tar.bz2);
- взять код из [репозитория Git](https://gitlab.gnome.org/GNOME/gimp/-/tags/GIMP_2_10_20).

Вам также понадобятся библиотеки babl и GEGL, разрабатываемые командой. Подробнее о сборке GIMP из исходного кода вы можете прочитать в [этой статье](/pages/participate/building-from-git/).
